use Mix.Config

defmodule Case do
	defstruct [:id, :name]
end

config(:scenarios, :setup,
  holder_module: Case,
  holder_name: :case,
  repo: Test.Repo,
	illustration_uploader: Uploader,
	calculations_module: DFLCalculations)
