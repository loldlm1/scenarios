use Mix.Config

config(:scenarios, :setup,
  holder_module: Scenarios.Holder,
  holder_name: :prospect,
  repo: Scenarios.Repo,
	illustration_uploader: Uploader,
	calculations_module: DFLCalculations)


config :scenarios, ecto_repos: [Scenarios.Repo]

config :scenarios, Scenarios.Repo,
  adapter: Ecto.Adapters.Postgres,
  pool: Ecto.Adapters.SQL.Sandbox,
  database: "scenarios_test",
  username: System.get_env("PGUSER") || "postgres",
  password: System.get_env("PGPASSWORD") || "postgres"

config :logger, :console, level: :error
