# Scenarios


## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `scenarios` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:scenarios, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/scenarios](https://hexdocs.pm/scenarios).

## Setup

The configuration to dynamically works on the project added.

```elixir
config(:scenarios, :setup,
  holder_module: Scenarios.Holder,
  holder_name: :prospect,
  repo: Scenarios.Repo,
  illustration_uploader: Uploader, # GCS uploader 
  calculations_module: DFLCalculations
)
```

After the setup config is added to your `config.ex` file, add the migration file.

```elixir
defmodule Scenarios.Repo.CreateScenariosTables do
  import Scenarios.Migration
  use Scenarios.Migration
end
```

Run the migrations and start to use the library modules.

```elixir
# Modules

- Scenarios.Scenarios
- Scenarios.Illustrations
- Scenarios.Utils
```
