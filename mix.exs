defmodule Scenarios.MixProject do
  use Mix.Project

  def project do
    [
      app: :scenarios,
      version: "0.1.0",
      elixir: "~> 1.10",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      elixirc_paths: elixirc_paths(Mix.env)
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:ecto_sql, "~> 3.0"},
      {:postgrex, ">= 0.0.0"},
      {:ecto_enum, "~> 1.4"},
      {:oban, "~> 2.1"},
      {:phoenix_pubsub, "~> 2.0"},
      {:httpoison, "~> 1.7"},
      {:plug, "~> 1.10.3"},
      {:briefly, "~> 0.3"},
      {:muontrap, "~> 0.4"},
      {:faker, "~> 0.10", only: [:dev, :test]},
      {:mock, "~> 0.3.0", only: [:test]},
      {:stream_data, "~> 0.1", only: :test},
      {:ex_machina, "~> 2.4", only: [:dev, :test]}
    ]
  end
end
