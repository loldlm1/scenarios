"""Identify the type of a pdf file"""

import logging
import re
import sys
import os
import pdftotext
import erlang
from PyPDF2 import PdfFileReader

logging.basicConfig(stream=sys.stderr, level=logging.WARNING)

FILE_PATH = sys.argv[1]


def cover_page_contains(the_pdf_text_pages, search_text):
    """Check if a cover page containing the search text exists"""
    return bool(re.search(search_text, the_pdf_text_pages[0]))

def page_header_contains(the_pdf_text_page, search_text):
    return bool(re.search(search_text, the_pdf_text_page))

file = open(FILE_PATH, "rb")
filename = file.name

RESPONSE = None

try:
    PDF = PdfFileReader(file)
    if PDF.isEncrypted:
        try:
            PDF.decrypt('')
            print('File Decrypted (PyPDF2)')
        except:
            command="cp "+filename+" temp.pdf; qpdf --password='' --decrypt temp.pdf "+filename
            os.system(command)
            print('File Decrypted (qpdf)')
            #re-open the decrypted file
            fp = open(filename, "rb")
            PDF = PdfFileReader(fp)
    else:
        print('File Not Encrypted')

    PDF_TEXT_PAGES = pdftotext.PDF(open(FILE_PATH, "rb"))

    OUTLINES = PDF.getOutlines()


    if cover_page_contains(PDF_TEXT_PAGES, 'Foresters Advantage'):
        RESPONSE = (erlang.OtpErlangAtom('ok'),
                    erlang.OtpErlangAtom('foresters'))
    if cover_page_contains(PDF_TEXT_PAGES, 'The Lafayette Life Insurance Company'):
        RESPONSE = (erlang.OtpErlangAtom('ok'),
                    erlang.OtpErlangAtom('lafayette'))
    if cover_page_contains(PDF_TEXT_PAGES, 'Horizon Value'):
        RESPONSE = (erlang.OtpErlangAtom('ok'),
                    erlang.OtpErlangAtom('mutual_trust'))

    if RESPONSE is None:
        RESPONSE = (erlang.OtpErlangAtom('error'), 'No type identified')
except:
        RESPONSE = (erlang.OtpErlangAtom('error'), 'Something went wrong')

sys.stdout.buffer.write(erlang.term_to_binary(RESPONSE))
