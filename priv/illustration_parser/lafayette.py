import camelot
import json
import logging
import sys
import erlang
import pdftotext
import re
import math
from PyPDF2 import PdfFileReader
from tabula import read_pdf
logging.basicConfig(stream=sys.stderr, level=logging.WARNING)

flatten = lambda l: [item for sublist in l for item in sublist]

file_path = sys.argv[1]

INTEGER_FIELD = "INTEGER"
DECIMAL_FIELD = "DECIMAL"

def get_total_lump_sum(pdf_text):
  for index, page in enumerate(pdf_text):
    if ("Tabular Detail" in page and "Non-Guaranteed Assumptions" in page):
      page = re.split('[ ]{2,}|\n', pdf_text[index])
      page = [value for value in page if value]
      for index, value in enumerate(page):
        if value == 'Total Lump Sum:':
          return parsed_data(page[index + 1])

def get_illustration_product(pdf_text):
  page = re.split('[ ]{2,}|\n', pdf_text[0])
  page = [value for value in page if value]
  for index, value in enumerate(page):
    if value == 'A Basic Life Insurance Illustration of':
      return page[index + 1]

def get_illustration_date(pdf_text):
  page = pdf_text[0].split()
  for index, value in enumerate(page):
    if (value == 'PM' or value == 'AM'):
      date = [page[index - 4], page[index - 3], page[index - 2]]
      return ' '.join(date)

def page_range_string(first, last):
    return "%s-%s" %(first, last)

def get_tabular_details_values_pages(pdf_text):
    tabular_pages = []
    for index, page in enumerate(pdf_text):
      if ("Tabular Detail" in page and "Non-Guaranteed Assumptions" in page):
        tabular_pages.append(str(index + 1))
    return page_range_string(tabular_pages[0], tabular_pages[-1])

def is_int(x):
    try:
        int(x)
        return True
    except:
        return False

def field_type(cell):
    if cell.find('%') != -1:
        return DECIMAL_FIELD;
    else:
        return INTEGER_FIELD

def parsed_data(cell):
    if field_type(cell) == DECIMAL_FIELD:
        return float(cell
                     .replace('$', '')
                     .replace(',', '')
                     .replace('%', '')
                     .replace('(', '')
                     .replace(')', '')
                     .replace('M', ''))
    else:
        return int(float(cell
                         .replace('$', '')
                         .replace(',', '')
                         .replace('%', '')
                         .replace('(', '')
                         .replace(')', '')
                         .replace('M', '')))


def isNaN(num):
  try:
    return math.isnan(num)
  except:
    return False

def hasNumbers(inputString):
    return any(char.isdigit() for char in inputString)

def parse_table_data(table, type='camelot'):
  parsed_table = []
  table_data = flatten([table.data for table in table]) if type == 'camelot' else table

  for row in table_data:
    new_row = []
    for field in row:
      if field == 'Lapse':
        new_row.append('0')
      elif field != '' and isNaN(field) == False:
        values = field.split()
        v_ordered = []
        [v_ordered.append(val) for val in values if hasNumbers(val)]

        [new_row.append(field) for field in v_ordered if field != '']

    if len(new_row) > 0 and len(new_row) <= 8 and is_int(new_row[0]):
      for index, field in enumerate(new_row):
        if index == 4:
          [new_row.insert(index, '0') for _ in range(0, 3)]
          parsed_table.append(new_row)
    elif len(new_row) > 5 and is_int(new_row[0]):
      parsed_table.append(new_row)

  return parsed_table


def get_tabular_rows(tabular_rows):
  tabular_details = []
  for row in tabular_rows:
    tabular_details.append({
      'age': parsed_data(row[0]),
      'year': parsed_data(row[1]),
      'contract_premium_guaranteed': parsed_data(row[2]),
      'net_cash_value_guaranteed': parsed_data(row[3]),
      'death_benefit_guaranteed': parsed_data(row[4]),
      'premium_outlay_non_guaranteed': parsed_data(row[5]),
      'surr_to_pay_prem_non_guaranteed': parsed_data(row[6]),
      'annual_dividend_non_guaranteed': parsed_data(row[7]),
      'increase_in_net_cash_value_non_guaranteed': parsed_data(row[8]),
      'net_cash_value_non_guaranteed': parsed_data(row[9]),
      'death_benefit_non_guaranteed': parsed_data(row[10]),
      '_type': 'lafayette'
    })
  return tabular_details

def by_year(row):
  return int(row[0])

def verify_tables(camelot_table, tabula_table):
  tabula_table = [row for row in tabula_table if len(row) > 3 and len(row) <= 10]
  diff = [i for i in camelot_table + tabula_table if i not in camelot_table or i not in tabula_table]
  [camelot_table.append(row) for row in diff]
  camelot_table.sort(key=by_year)
  table = [val for index, val in enumerate(camelot_table) if val not in camelot_table[:index]]
  return table


PDF_TEXT_PAGES = pdftotext.PDF(open(file_path, "rb"))

pdf = PdfFileReader(open(file_path, "rb"))

tabular_details_pages = get_tabular_details_values_pages(PDF_TEXT_PAGES)

tabular_values_table = camelot.read_pdf(file_path, flavor='stream',
                          pages=tabular_details_pages)
tabular_values_table_data = parse_table_data(tabular_values_table)

tabular_values_table_tabula = read_pdf(open(file_path, "rb"), pages=tabular_details_pages, encoding='ISO-8859-1', multiple_tables=True, silent=True)
tabular_values_table_tabula = [row.values.tolist() for row in tabular_values_table_tabula]
tabular_values_table_tabula = flatten(tabular_values_table_tabula)
tabular_values_table_tabula_data = parse_table_data(tabular_values_table_tabula, 'tabula')

tabular_values_table_data = verify_tables(tabular_values_table_data, tabular_values_table_tabula_data)

response = {}
response['date'] = get_illustration_date(PDF_TEXT_PAGES)
response['product_name'] = get_illustration_product(PDF_TEXT_PAGES)
response['carrier_name'] = 'The Lafayette Life Insurance Company'
response['total_lump_sum'] = get_total_lump_sum(PDF_TEXT_PAGES) or 0
response['tabular_details'] = get_tabular_rows(tabular_values_table_data)

sys.stdout.buffer.write(erlang.term_to_binary(response))
