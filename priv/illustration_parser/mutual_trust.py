import camelot
import json
import logging
import sys
import erlang
import pdftotext
import re
import math
from PyPDF2 import PdfFileReader
from tabula import read_pdf
logging.basicConfig(stream=sys.stderr, level=logging.WARNING)

flatten = lambda l: [item for sublist in l for item in sublist]

file_path = sys.argv[1]

INTEGER_FIELD = "INTEGER"
DECIMAL_FIELD = "DECIMAL"

def get_total_lump_sum(tabula_table):
  return (parsed_data(tabula_table[0][2]) - parsed_data(tabula_table[1][2]))

def get_illustration_product(pdf_text):
  page = re.split('[ ]{2,}|\n', pdf_text[0])
  page = [value for value in page if value]
  for index, value in enumerate(page):
    if value == 'A Life Insurance Policy Illustration':
      return (page[index + 2] + " " + page[index + 1])

def get_illustration_date(pdf_text):
  page = pdf_text[0].split()
  for index, value in enumerate(page):
    if (value == 'Form:'):
      date = [page[index - 3], page[index - 2], page[index - 1]]
      return ' '.join(date)

def page_range_string(first, last):
    return "%s-%s" %(first, last)

def get_tabular_details_values_pages(pdf_text):
    tabular_pages = []
    for index, page in enumerate(pdf_text):
      if ("Basic Illustration" in page and "Total Values Including" in page):
        tabular_pages.append(str(index + 1))
    return page_range_string(tabular_pages[0], tabular_pages[-1])

def is_int(x):
    try:
        int(x)
        return True
    except:
        return False

def field_type(cell):
    if cell.find('%') != -1:
        return DECIMAL_FIELD;
    else:
        return INTEGER_FIELD

def parsed_data(cell):
    if field_type(cell) == DECIMAL_FIELD:
        return float(cell
                     .replace('$', '')
                     .replace(',', '')
                     .replace('%', '')
                     .replace('(', '')
                     .replace(')', '')
                     .replace('M', ''))
    else:
        return int(float(cell
                         .replace('$', '')
                         .replace(',', '')
                         .replace('%', '')
                         .replace('(', '')
                         .replace(')', '')
                         .replace('M', '')))


def isNaN(num):
  try:
    return math.isnan(num)
  except:
    return False

def mutual_year(field):
  try:
    return int(field.replace(' ', ''))
  except:
    return 0


def hasNumbers(inputString):
    return any(char.isdigit() for char in inputString)

def zero_between_fields(row, index):
  try:
    return (hasNumbers(row[index + 1]) and hasNumbers(row[index - 1]))
  except:
    return False

def has_zeros(row, index):
  try:
    return row[index] in ['00', '000', '0000', '00000', '000000']
  except:
    return False

def parse_table_data(table, type='camelot'):
  parsed_table = []
  table_data = flatten([table.data for table in table]) if type == 'camelot' else table
  year = 1

  for row_index, row in enumerate(table_data):
    new_row = []
    for index, field in enumerate(row):
      if index == 0 and is_int(row[1]) and (field == '' or mutual_year(field) > 200):
        new_row.append(str(year))
        year += 1
      elif field == '' and zero_between_fields(row, index):
        new_row.append('0')
      elif field != '' and isNaN(field) == False:
        values = field.split()
        v_ordered = []
        [v_ordered.append(val) for val in values if hasNumbers(val)]

        [new_row.append(field) for field in v_ordered if field != '']

    if len(new_row) > 5 and is_int(new_row[0]):
      zero_indexes = [index for index, field in enumerate(new_row) if field in ['00', '000', '0000', '00000', '000000']]
      new_row = [field for field in new_row if field not in ['00', '000', '0000', '00000', '000000']]

      if len(new_row) == 9:
        new_row.insert(2, '0')
        new_row.insert(6, '0')

      parsed_table.append(new_row)

  return parsed_table


def get_tabular_rows(tabular_rows):
  tabular_details = []
  for row in tabular_rows:
    tabular_details.append({
      'end_of_year': parsed_data(row[0]),
      'age': parsed_data(row[1]),
      'annualized_contract_premium_guaranteed': parsed_data(row[2]),
      'increase_in_cash_surrender_value_guaranteed': parsed_data(row[3]),
      'cash_surrender_value_guaranteed': parsed_data(row[4]),
      'death_benefit_guaranteed': parsed_data(row[5]),
      'annualized_contract_premium_non_guaranteed': parsed_data(row[6]),
      'annual_dividend_non_guaranteed': parsed_data(row[7]),
      'increase_in_cash_surrender_value_non_guaranteed': parsed_data(row[8]),
      'cash_surrender_value_non_guaranteed': parsed_data(row[9]),
      'death_benefit_non_guaranteed': parsed_data(row[10]),
      '_type': 'mutual_trust'
    })
  return tabular_details

def by_year(row):
  return int(row[0])

def verify_tables(camelot_table, tabula_table):
  tabula_table = [row for row in tabula_table if len(row) > 3 and len(row) <= 10]
  diff = [i for i in camelot_table + tabula_table if i not in camelot_table or i not in tabula_table]
  [camelot_table.append(row) for row in diff]
  camelot_table.sort(key=by_year)
  table = [val for index, val in enumerate(camelot_table) if val not in camelot_table[:index]]
  return table


PDF_TEXT_PAGES = pdftotext.PDF(open(file_path, "rb"))

pdf = PdfFileReader(open(file_path, "rb"))

tabular_details_pages = get_tabular_details_values_pages(PDF_TEXT_PAGES)

tabular_values_table = camelot.read_pdf(file_path, flavor='stream',
                          pages=tabular_details_pages)
tabular_values_table_data = parse_table_data(tabular_values_table)

tabular_values_table_tabula = read_pdf(open(file_path, "rb"), pages=tabular_details_pages, encoding='ISO-8859-1', multiple_tables=True, silent=True)
tabular_values_table_tabula = [row.values.tolist() for row in tabular_values_table_tabula]
tabular_values_table_tabula = flatten(tabular_values_table_tabula)
tabular_values_table_tabula_data = parse_table_data(tabular_values_table_tabula, 'tabula')

tabular_values_table_data = verify_tables(tabular_values_table_data, tabular_values_table_tabula_data)

response = {}
response['date'] = get_illustration_date(PDF_TEXT_PAGES)
response['product_name'] = get_illustration_product(PDF_TEXT_PAGES)
response['carrier_name'] = 'MUTUAL TRUST LIFE INSURANCE COMPANY'
response['total_lump_sum'] = get_total_lump_sum(tabular_values_table_data)
response['tabular_details'] = get_tabular_rows(tabular_values_table_data)

sys.stdout.buffer.write(erlang.term_to_binary(response))
