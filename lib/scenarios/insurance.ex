defmodule Scenarios.Insurance do
  @moduledoc """
  Embedded schema for Scenario.Insurance
  """
  use Ecto.Schema
  import Ecto.Changeset
  alias Scenarios.Utils

  defmodule Type do
    @moduledoc false
    use EctoEnum,
      permanent: "permanent",
      term: "term"
  end

  defmodule Rating do
    @moduledoc false
    use EctoEnum,
      standard: "standard",
      standard_non_tobacco: "standard_non_tobacco",
      tobacco: "tobacco"
  end

  embedded_schema do
    field(:name, :string)
    field(:monthly_premium, FormattedDecimal)
    field(:cash_value, FormattedDecimal)
    field(:death_benefit, FormattedDecimal)
    field(:rating, Rating)
    field(:type, Type)
  end

  @params ~w(name monthly_premium cash_value death_benefit rating type)a
  def changeset(insurance, attrs, hard_validation \\ true) do
    insurance
    |> cast(attrs, @params)
    |> validate(hard_validation)
    |> Utils.put_action()
  end

  defp validate(changeset, true) do
    changeset
    |> Utils.force_change(@params)
    |> validate_required(@params)
    |> validate_number(:monthly_premium, greater_than: 0)
    |> validate_number(:death_benefit, greater_than_or_equal_to: 0)
    |> validate_number(:cash_value, greater_than_or_equal_to: 0)
  end

  defp validate(changeset, false), do: changeset
end
