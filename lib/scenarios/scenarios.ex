defmodule Scenarios.Scenarios do
  @moduledoc """
  The Scenarios context.
  """

  import Ecto.Query, warn: false

  @holder_module Application.get_env(:scenarios, :setup) |> Keyword.get(:holder_module)
  @holder_name Application.get_env(:scenarios, :setup) |> Keyword.get(:holder_name)

  alias Scenarios.{Scenario, Total, Utils}
  alias Ecto.Multi

  def source(opts) do
    {preload, opts} = Keyword.pop(opts, :preload, [])

    opts
    |> with_deleted()
    |> for_user(opts)
    |> preload(^preload)
  end

  defp with_deleted(opts) do
    if Keyword.get(opts, :with_deleted) do
      from(s in Scenario, prefix: "public")
    else
      Scenario
    end
  end

  defp for_user(source, opts) do
    case Keyword.get(opts, :user_id) do
      id when is_binary(id) ->
        from(s in source, join: p in assoc(s, ^@holder_name), where: p.user_id == ^id)

      _ ->
        source
    end
  end

  @doc """
  Returns the list of scenarios.

  ## Examples

      iex> list_scenarios()
      [%Scenario{}, ...]

  """
  def list_scenarios do
    repo().all(Scenario)
  end

  @spec get_scenario(String.t(), [{:with_deleted, boolean()} | {:preload, [atom()]}]) ::
          Scenario.t() | nil
  @doc """
  Gets a single scenario.

  Raises `Ecto.NoResultsError` if the Scenario does not exist.

  ## Examples

      iex> get_scenario!(123)
      %Scenario{}

      iex> get_scenario!(456)
      ** (Ecto.NoResultsError)

  """
  def get_scenario(id, opts \\ []) do
    opts
    |> source()
    |> repo().get(id)
  end

  @doc """
  Creates a scenario.

  ## Examples

      iex> create_scenario(%{field: value})
      {:ok, %Scenario{}}

      iex> create_scenario(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_scenario(attrs) do
    Multi.new()
    |> Multi.insert(:scenario, Scenario.changeset(%Scenario{}, attrs, false))
    |> Multi.run(:update_holder, &update_holder/2)
    |> repo().transaction()
    |> handle_scenario()
  end

  @doc """
  Updates a scenario.

  ## Examples

      iex> update_scenario(scenario, %{field: new_value})
      {:ok, %Scenario{}}

      iex> update_scenario(scenario, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_scenario(scenario = %Scenario{}, attrs) do
    Multi.new()
    |> Multi.update(:scenario, Scenario.changeset(scenario, attrs, false))
    |> Multi.run(:update_holder, &update_holder/2)
    |> repo().transaction()
    |> handle_scenario()
  end

  defp handle_scenario({:ok, %{scenario: scenario}}), do: {:ok, scenario}
  defp handle_scenario({:error, _, changeset, _}), do: {:error, changeset}

  defp update_holder(_repo, %{scenario: scenario}) do
    holder_id = scenario |> Map.get(:"#{@holder_name}_id")
    holder_key = :"#{@holder_name}_id"

    base =
      from(
        p in @holder_module,
        left_join: s in Scenario,
        on: field(s, ^:"#{@holder_name}_id") == p.id and s.submitted_to_carrier == true,
        where: p.id == ^holder_id,
        select: %{:holder_id => p.id, :submitted_to_carrier => count(s.id) > 0},
        group_by: p.id
      )
      |> subquery()

    from(p in @holder_module,
      join: s in ^base,
      on: field(s, :holder_id) == p.id,
      select: p,
      update: [set: [submitted_to_carrier: s.submitted_to_carrier]]
    )
    |> repo().update_all([])
    |> case do
      {1, [holder]} ->
        {:ok, holder}
      _ -> {:error, :could_not_update_holder}
    end
  end

  @doc """
  Deletes a scenario.

  ## Examples

      iex> delete_scenario(scenario)
      {:ok, %Scenario{}}

      iex> delete_scenario(scenario)
      {:error, %Ecto.Changeset{}}

  """
  def delete_scenario(scenario = %Scenario{}) do
    scenario
    |> Ecto.Changeset.change(%{deleted_at: DateTime.utc_now()})
    |> repo().update()
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking scenario changes.

  ## Examples

      iex> change_scenario(scenario)
      %Ecto.Changeset{source: %Scenario{}}

  """
  def change_scenario(scenario = %Scenario{}), do: change_scenario(scenario, %{})
  def change_scenario(params), do: change_scenario(%Scenario{}, params)

  def change_scenario(scenario = %Scenario{}, params) do
    Scenario.changeset(scenario, params, true)
  end

  def next_scenario_name(%@holder_module{id: id}) do
    from(p in @holder_module,
      left_join: s in assoc(p, :scenarios),
      prefix: "public",
      where: p.id == ^id,
      group_by: [p.first_name, p.last_name],
      select:
        fragment("? || ' ' || ? || ' scenario ' || ?", p.first_name, p.last_name, count(s.id) + 1)
    )
    |> repo().one()
  end

  def next_scenario(holder = %@holder_module{id: id}) do
    %{
      "#{@holder_name}_id": id,
      name: next_scenario_name(holder),
      dfl_start_date: Date.utc_today()
    }
    |> create_scenario()
  end

  defdelegate total(scenario), to: Total, as: :build
  defdelegate sort_debts(debts, mortgages, type), to: Utils
  defdelegate actual_payoff_order(debts, mortgages), to: Utils

  def can_generate_report?(_scenario, nil), do: false
  def can_generate_report?(%Scenario{dfl_start_date: nil}, _illustration), do: false
  def can_generate_report?(%Scenario{}, _illustration), do: true

  defp repo do
    Application.get_env(:scenarios, :setup) |> Keyword.get(:repo)
  end
end
