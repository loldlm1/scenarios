defmodule Scenarios.Total do
  @moduledoc """
  Summarize scenario's investments, mortgages and insurances
  """
  alias Scenarios.{
    Insurance,
    Investment,
    Mortgage,
    Scenario
  }

  defstruct lines: [], summary: [], scenario: nil

  defmodule Line do
    @moduledoc """
    Represent the convertion of investments, mortgages and insurances the total module
    """
    @type t :: %__MODULE__{
            account: String.t(),
            total_value: integer,
            monthly_contribution: integer,
            source: :investment | :mortgage | :insurance
          }

    defstruct [
      :account,
      :total_value,
      :monthly_contribution,
      :source
    ]
  end

  defmodule Summary do
    @moduledoc """
    Summarize total lines
    """
    @type t :: %__MODULE__{
            total_cash: integer,
            monthly_contribution: integer
          }
    defstruct total_cash: 0, monthly_contribution: 0
  end

  @type t :: %__MODULE__{
          summary: Summary.t(),
          lines: [Line.t()],
          scenario: Scenario.t()
        }
  @spec build(Scenario.t()) :: t()
  @doc """
  Genrates total record based on scenario data
  """
  def build(scenario = %Scenario{}) do
    scenario
    |> new()
    |> add_investments()
    |> add_mortgages()
    |> add_insurances()
    |> reverse_lines()
    |> calculate_summary()
  end

  defp new(scenario), do: %__MODULE__{scenario: scenario}
  def add_line(total = %__MODULE__{}, [], _builder), do: total

  def add_line(total = %__MODULE__{}, to_add, builder) do
    Enum.reduce(to_add, total, fn line, total ->
      line = builder.(line)
      %__MODULE__{total | lines: [line | total.lines]}
    end)
  end

  defp add_investments(totals = %__MODULE__{scenario: scenario}) do
    # line <- Investment
    # account <- name
    # monthly_contribution <- monthly_contribution
    # total_cash <- account_value
    # monthly_contribution <- monthly_contribution
    # total_value <- account_value

    add_line(totals, scenario.investments, fn
      %Investment{
        name: name,
        monthly_contribution: monthly_contribution,
        account_value: account_value
      } ->
        %Line{
          account: name,
          monthly_contribution: monthly_contribution,
          total_value: account_value,
          source: :investment
        }
    end)
  end

  defp add_mortgages(totals = %__MODULE__{scenario: scenario}) do
    # line <- Mortgage:
    # account <- name
    # monthly_contribution <- monthly_payment
    # monthly_contribution <- monthly_contribution
    # total_cash <- 0
    # total_value <- 0

    add_line(totals, scenario.mortgages, fn
      %Mortgage{
        name: name,
        monthly_payment: monthly_payment,
        principal: principal
      } ->
        %Line{
          account: name,
          monthly_contribution: monthly_payment,
          total_value: principal,
          source: :mortgage
        }
    end)
  end

  defp add_insurances(totals = %__MODULE__{scenario: scenario}) do
    add_line(totals, scenario.insurances, fn
      # account <- name
      # monthly_contribution <- monthly_contribution
      # monthly_contribution <- monthly_premium_redirecting
      # total_cash <- total_value_redirecting
      # total_value: <-  total_value
      %Insurance{
        monthly_premium: monthly_contribution,
        cash_value: total_value,
        name: name
      } ->
        %Line{
          account: name,
          monthly_contribution: monthly_contribution,
          total_value: total_value,
          source: :insurance
        }
    end)
  end

  defp reverse_lines(total = %__MODULE__{lines: lines}) do
    %__MODULE__{total | lines: Enum.reverse(lines)}
  end

  defp calculate_summary(total = %__MODULE__{lines: lines}) do
    summary =
      Enum.reduce(lines, %Summary{}, fn line, summary ->
        %Summary{
          total_cash: add(line.total_value, summary.total_cash),
          monthly_contribution: add(line.monthly_contribution, summary.monthly_contribution)
        }
      end)

    %__MODULE__{total | summary: summary}
  end

  defp add(left, right) when is_number(left), do: left + right
  defp add(_left, right), do: right
end
