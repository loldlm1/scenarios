defmodule Scenarios.IllustrationRow do
  @moduledoc """
  Ecto Type that transform parsed illustration to the correspnding type.
  Also, it will dump and load it from the database and converting it base the target struct
  """
  use Ecto.Type

  defmodule Foresters do
    @moduledoc """
    Illustration struct for Foresters type
    """
    defstruct [
      :end_of_year,
      :age,
      :contract_premium,
      :guar_cash_value,
      :guar_death_benefit,
      :annual_dividend,
      :premium_outlay,
      :accum_paid_up_add,
      :paid_up_cash_value,
      :total_cash_value,
      :total_death_benefit
    ]
  end

  defmodule Lafayette do
    @moduledoc """
    Illustration struct for Lafayette type
    """
    defstruct [
      :age,
      :year,
      :contract_premium_guaranteed,
      :net_cash_value_guaranteed,
      :death_benefit_guaranteed,
      :premium_outlay_non_guaranteed,
      :surr_to_pay_prem_non_guaranteed,
      :annual_dividend_non_guaranteed,
      :increase_in_net_cash_value_non_guaranteed,
      :net_cash_value_non_guaranteed,
      :death_benefit_non_guaranteed
    ]
  end

  defmodule MutualTrust do
    @moduledoc """
    Illustration struct for MutualTrust type
    """
    defstruct [
      :end_of_year,
      :age,
      :annualized_contract_premium_guaranteed,
      :increase_in_cash_surrender_value_guaranteed,
      :cash_surrender_value_guaranteed,
      :death_benefit_guaranteed,
      :annualized_contract_premium_non_guaranteed,
      :annual_dividend_non_guaranteed,
      :increase_in_cash_surrender_value_non_guaranteed,
      :cash_surrender_value_non_guaranteed,
      :death_benefit_non_guaranteed
    ]
  end

  def type, do: :map

  def cast(row = %{'_type' => _}) do
    row
    |> Enum.map(fn
      {key, value} when is_list(value) -> {List.to_string(key), List.to_string(value)}
      {key, value} -> {List.to_string(key), value}
    end)
    |> Map.new()
    |> cast()
  end

  def cast(row = %{"_type" => "mutual_trust"}) do
    {:ok,
     %MutualTrust{
       end_of_year: get_integer(row, "end_of_year"),
       age: get_integer(row, "age"),
       annualized_contract_premium_guaranteed:
         get_decimal(row, "annualized_contract_premium_guaranteed"),
       increase_in_cash_surrender_value_guaranteed:
         get_decimal(row, "increase_in_cash_surrender_value_guaranteed"),
       cash_surrender_value_guaranteed: get_decimal(row, "cash_surrender_value_guaranteed"),
       death_benefit_guaranteed: get_decimal(row, "death_benefit_guaranteed"),
       annualized_contract_premium_non_guaranteed:
         get_decimal(row, "annualized_contract_premium_non_guaranteed"),
       annual_dividend_non_guaranteed: get_decimal(row, "annual_dividend_non_guaranteed"),
       increase_in_cash_surrender_value_non_guaranteed:
         get_decimal(row, "increase_in_cash_surrender_value_non_guaranteed"),
       cash_surrender_value_non_guaranteed:
         get_decimal(row, "cash_surrender_value_non_guaranteed"),
       death_benefit_non_guaranteed: get_decimal(row, "death_benefit_non_guaranteed")
     }}
  end

  def cast(row = %{"_type" => "foresters"}) do
    {:ok,
     %Foresters{
       end_of_year: get_integer(row, "end_of_year"),
       age: get_integer(row, "age"),
       contract_premium: get_decimal(row, "contract_premium"),
       guar_cash_value: get_decimal(row, "guar_cash_value"),
       guar_death_benefit: get_decimal(row, "guar_death_benefit"),
       annual_dividend: get_decimal(row, "annual_dividend"),
       premium_outlay: get_decimal(row, "premium_outlay"),
       accum_paid_up_add: get_decimal(row, "accum_paid_up_add"),
       paid_up_cash_value: get_decimal(row, "paid_up_cash_value"),
       total_cash_value: get_decimal(row, "total_cash_value"),
       total_death_benefit: get_decimal(row, "total_death_benefit")
     }}
  end

  def cast(row = %{"_type" => "lafayette"}) do
    {:ok,
     %Lafayette{
       age: get_integer(row, "age"),
       year: get_integer(row, "year"),
       contract_premium_guaranteed: get_decimal(row, "contract_premium_guaranteed"),
       net_cash_value_guaranteed: get_decimal(row, "net_cash_value_guaranteed"),
       death_benefit_guaranteed: get_decimal(row, "death_benefit_guaranteed"),
       premium_outlay_non_guaranteed: get_decimal(row, "premium_outlay_non_guaranteed"),
       surr_to_pay_prem_non_guaranteed: get_decimal(row, "surr_to_pay_prem_non_guaranteed"),
       annual_dividend_non_guaranteed: get_decimal(row, "annual_dividend_non_guaranteed"),
       increase_in_net_cash_value_non_guaranteed:
         get_decimal(row, "increase_in_net_cash_value_non_guaranteed"),
       net_cash_value_non_guaranteed: get_decimal(row, "net_cash_value_non_guaranteed"),
       death_benefit_non_guaranteed: get_decimal(row, "death_benefit_non_guaranteed")
     }}
  end

  def cast(row = %MutualTrust{}), do: {:ok, row}
  def cast(row = %Foresters{}), do: {:ok, row}
  def cast(row = %Lafayette{}), do: {:ok, row}

  # Everything else is a failure though
  def cast(_), do: :error

  def cast!(data) do
    case cast(data) do
      :error -> raise "unsupported data type #{inspect(data)}"
      {:ok, struct} -> struct
    end
  end

  # When loading data from the database, as long as it's a map,
  # we just put the data back into an each illustration struct to be stored in
  # the loaded schema struct.
  def load(data = %{"_type" => _type}), do: cast(data)

  def dump(data = %type{}) do
    type =
      case type do
        MutualTrust -> "mutual_trust"
        Foresters -> "foresters"
        Lafayette -> "lafayette"
      end

    {:ok, data |> Map.from_struct() |> Map.put("_type", type)}
  end

  def dump(_), do: :error

  def dump!(data) do
    case dump(data) do
      {:ok, data} -> data
      :error -> raise "cannot dump #{inspect(data)}"
    end
  end

  defp get_decimal(row, field) do
    row
    |> Map.get(field)
    |> case do
      float when is_float(float) -> Decimal.from_float(float)
      nil -> nil
      number -> Decimal.new(number)
    end
  end

  defp get_integer(row, field) do
    row
    |> Map.get(field)
    |> case do
      integer when is_integer(integer) -> integer
      nil -> nil
      number -> String.to_integer(number)
    end
  end

  for type <- [Foresters, MutualTrust, Lafayette] do
    defimpl Jason.Encoder, for: type do
      alias Scenarios.IllustrationRow

      def encode(value, opts) do
        value
        |> IllustrationRow.dump!()
        |> Jason.Encode.map(opts)
      end
    end
  end
end
