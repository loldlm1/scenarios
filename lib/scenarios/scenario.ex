defmodule Scenarios.Scenario do
  @moduledoc """
    Schema for Scenario
  """

  use Data, :model

  @holder_module Application.get_env(:scenarios, :setup) |> Keyword.get(:holder_module)
  @holder_name Application.get_env(:scenarios, :setup) |> Keyword.get(:holder_name)

  alias Scenarios.{
    Debt,
    Illustration,
    IncomeSource,
    Insurance,
    Investment,
    Mortgage,
    Utils
  }

  defmodule PayoffOrder do
    @moduledoc false

    use EctoEnum,
      debt_snowball: "debt_snowball",
      debt_avalanche: "debt_avalanche",
      custom: "custom"
  end

  @type t() :: %__MODULE__{
          name: String.t(),
          goals: String.t(),
          payoff_order: PayoffOrder.t(),
          savings_account_interest_rate: Decimal.t(),
          submitted_to_carrier: boolean(),
          debts: [Debt.t()],
          income_sources: [IncomeSource.t()],
          investments: [Investment.t()],
          mortgages: [Mortgage.t()],
          insurances: [Insurance.t()],
          illustrations: [Illustration.t()],
          total_debt_overpayment: Decimal.t(),
          total_insurance_monthly_premium_redirecting: Decimal.t(),
          total_insurance_cash_value_redirecting: Decimal.t(),
          total_investments_monthly_premium_redirecting: Decimal.t(),
          total_investments_cash_value_redirecting: Decimal.t(),
          debt_overpayment_available: Decimal.t(),
          insurance_monthly_premium_redirecting_available: Decimal.t(),
          insurance_cash_value_redirecting_available: Decimal.t(),
          investments_monthly_premium_redirecting_available: Decimal.t(),
          investments_cash_value_redirecting_available: Decimal.t(),
          mortgage_overpayment_redirecting_available: Decimal.t(),
          total_mortgage_overpayment_redirecting: Decimal.t(),
          income_redirecting_available: Decimal.t(),
          total_income_redirecting: Decimal.t(),
          dfl_start_date: Date.t() | nil
        }
  @schema_prefix "filtered"

  schema "scenarios" do
    field(:goals, :string)
    field(:name, :string)
    field(:dfl_start_date, :date)
    field(:payoff_order, PayoffOrder, default: :debt_snowball)
    field(:savings_account_interest_rate, FormattedDecimal, default: Decimal.new("2.0"))
    field(:total_debt_overpayment, FormattedDecimal, default: Decimal.new("0.0"))

    field(:total_insurance_monthly_premium_redirecting, FormattedDecimal,
      default: Decimal.new("0.0")
    )

    field(:total_insurance_cash_value_redirecting, FormattedDecimal, default: Decimal.new("0.0"))

    field(:total_investments_monthly_premium_redirecting, FormattedDecimal,
      default: Decimal.new("0.0")
    )

    field(:total_investments_cash_value_redirecting, FormattedDecimal, default: Decimal.new("0.0"))

    field(:debt_overpayment_available, FormattedDecimal, default: Decimal.new("0.0"))

    field(:insurance_monthly_premium_redirecting_available, FormattedDecimal,
      default: Decimal.new("0.0")
    )

    field(:insurance_cash_value_redirecting_available, FormattedDecimal,
      default: Decimal.new("0.0")
    )

    field(:investments_monthly_premium_redirecting_available, FormattedDecimal,
      default: Decimal.new("0.0")
    )

    field(:investments_cash_value_redirecting_available, FormattedDecimal,
      default: Decimal.new("0.0")
    )

    field(:mortgage_overpayment_redirecting_available, FormattedDecimal, default: Decimal.new("0"))

    field(:total_mortgage_overpayment_redirecting, FormattedDecimal, default: Decimal.new("0"))
    field(:income_redirecting_available, FormattedDecimal, default: Decimal.new("0"))
    field(:total_income_redirecting, FormattedDecimal, default: Decimal.new("0"))
    field(:submitted_to_carrier, :boolean, default: false)
    embeds_many(:debts, Debt, on_replace: :delete)
    embeds_many(:income_sources, IncomeSource, on_replace: :delete)
    embeds_many(:investments, Investment, on_replace: :delete)
    embeds_many(:insurances, Insurance, on_replace: :delete)
    embeds_many(:mortgages, Mortgage, on_replace: :delete)
    has_many(:illustrations, Illustration)
    belongs_to(@holder_name, @holder_module)
    field(:deleted_at, :utc_datetime_usec)
    field(:is_valid, :boolean)
    timestamps()
  end

  @optional_params ~w(name goals savings_account_interest_rate
                      dfl_start_date
                      submitted_to_carrier deleted_at
                      total_debt_overpayment
                      total_insurance_monthly_premium_redirecting
                      total_insurance_cash_value_redirecting
                      total_investments_monthly_premium_redirecting
                      total_investments_cash_value_redirecting
                      total_mortgage_overpayment_redirecting
                      total_income_redirecting
                      mortgage_overpayment_redirecting_available
                      debt_overpayment_available
                      insurance_monthly_premium_redirecting_available
                      insurance_cash_value_redirecting_available
                      investments_monthly_premium_redirecting_available
                      investments_cash_value_redirecting_available
                      payoff_order)a
  @required_params ~w(#{@holder_name}_id name savings_account_interest_rate
                      total_debt_overpayment
                      total_insurance_monthly_premium_redirecting
                      total_insurance_cash_value_redirecting
                      total_investments_monthly_premium_redirecting
                      total_investments_cash_value_redirecting
                      total_mortgage_overpayment_redirecting
                      total_income_redirecting
                      submitted_to_carrier)a
  @doc false
  def changeset(scenario, attrs, hard_validation \\ true) do
    fields = cast_params(scenario)

    scenario
    |> cast(attrs, fields)
    |> cast_embed(:debts, with: &Debt.changeset(&1, &2, hard_validation))
    |> cast_embed(:income_sources, with: &IncomeSource.changeset(&1, &2, hard_validation))
    |> cast_embed(:investments, with: &Investment.changeset(&1, &2, hard_validation))
    |> cast_embed(:insurances, with: &Insurance.changeset(&1, &2, hard_validation))
    |> cast_embed(:mortgages, with: &Mortgage.changeset(&1, &2, hard_validation))
    |> calculate_total(:debt_overpayment_available, :total_debt_overpayment, :debts, :overrage)
    |> calculate_total(
      :insurance_monthly_premium_redirecting_available,
      :total_insurance_monthly_premium_redirecting,
      :insurances,
      :monthly_premium
    )
    |> calculate_total(
      :insurance_cash_value_redirecting_available,
      :total_insurance_cash_value_redirecting,
      :insurances,
      :cash_value
    )
    |> calculate_total(
      :investments_monthly_premium_redirecting_available,
      :total_investments_monthly_premium_redirecting,
      :investments,
      :monthly_contribution
    )
    |> calculate_total(
      :investments_cash_value_redirecting_available,
      :total_investments_cash_value_redirecting,
      :investments,
      :account_value
    )
    |> calculate_total(
      :mortgage_overpayment_redirecting_available,
      :total_mortgage_overpayment_redirecting,
      :mortgages,
      :monthly_overpayment_to_principal
    )
    |> calculate_total(
      :income_redirecting_available,
      :income_sources,
      :monthly_income
    )
    |> put_payoff_order()
    |> validate_required(@required_params)
    |> validate(hard_validation, fields)
    |> put_is_valid(attrs, hard_validation)
    |> Utils.put_action()
  end

  defp put_is_valid(changeset, _attrs, true) do
    put_change(changeset, :is_valid, changeset.valid?)
  end

  defp put_is_valid(changeset, attrs, false) do
    %{valid?: valid?} = changeset(changeset.data, attrs, true)

    put_change(changeset, :is_valid, valid?)
  end

  defp validate(changeset, true, fields) do
    changeset
    |> Utils.force_change(fields)
    |> Utils.fetch_and_validate_number(
      :total_debt_overpayment,
      less_than_or_equal_to: :debt_overpayment_available
    )
    |> Utils.fetch_and_validate_number(
      :total_insurance_monthly_premium_redirecting,
      less_than_or_equal_to: :insurance_monthly_premium_redirecting_available
    )
    |> Utils.fetch_and_validate_number(
      :total_insurance_cash_value_redirecting,
      less_than_or_equal_to: :insurance_cash_value_redirecting_available
    )
    |> Utils.fetch_and_validate_number(
      :total_investments_monthly_premium_redirecting,
      less_than_or_equal_to: :investments_monthly_premium_redirecting_available
    )
    |> Utils.fetch_and_validate_number(
      :total_investments_cash_value_redirecting,
      less_than_or_equal_to: :investments_cash_value_redirecting_available
    )
    |> Utils.fetch_and_validate_number(
      :total_income_redirecting,
      less_than_or_equal_to: :income_redirecting_available
    )
    |> Utils.fetch_and_validate_number(
      :total_mortgage_overpayment_redirecting,
      less_than_or_equal_to: :mortgage_overpayment_redirecting_available
    )
    |> validate_number(
      :total_debt_overpayment,
      greater_than_or_equal_to: 0
    )
    |> validate_number(
      :total_insurance_monthly_premium_redirecting,
      greater_than_or_equal_to: 0
    )
    |> validate_number(
      :total_insurance_cash_value_redirecting,
      greater_than_or_equal_to: 0
    )
    |> validate_number(
      :total_investments_monthly_premium_redirecting,
      greater_than_or_equal_to: 0
    )
    |> validate_number(
      :total_investments_cash_value_redirecting,
      greater_than_or_equal_to: 0
    )
    |> validate_number(
      :debt_overpayment_available,
      greater_than_or_equal_to: 0
    )
    |> validate_number(
      :insurance_monthly_premium_redirecting_available,
      greater_than_or_equal_to: 0
    )
    |> validate_number(
      :insurance_cash_value_redirecting_available,
      greater_than_or_equal_to: 0
    )
    |> validate_number(
      :investments_monthly_premium_redirecting_available,
      greater_than_or_equal_to: 0
    )
    |> validate_number(
      :investments_cash_value_redirecting_available,
      greater_than_or_equal_to: 0
    )
    |> validate_number(
      :income_redirecting_available,
      greater_than_or_equal_to: 0
    )
    |> validate_number(
      :total_income_redirecting,
      greater_than_or_equal_to: 0
    )
    |> validate_number(
      :mortgage_overpayment_redirecting_available,
      greater_than_or_equal_to: 0
    )
    |> validate_number(
      :total_mortgage_overpayment_redirecting,
      greater_than_or_equal_to: 0
    )
  end

  defp validate(changeset, false, _fields), do: changeset

  defp cast_params(%{id: id}) when is_nil(id), do: @required_params ++ @optional_params
  defp cast_params(_), do: @optional_params

  defp calculate_total(changeset, list, source_field) do
    changeset
    |> get_field(list)
    |> Enum.reduce(Decimal.new(0), fn record, total ->
      record |> get_or_zero(source_field) |> Decimal.add(total)
    end)
    |> do_round()
  end

  defp calculate_total(changeset, target_field, list, source_field) do
    total = calculate_total(changeset, list, source_field)

    put_change(changeset, target_field, total)
  end

  defp calculate_total(changeset, target_field, put_if_changed, list, source_field) do
    total = calculate_total(changeset, list, source_field)

    previous_value = get_field(changeset, target_field)

    with %Decimal{} <- previous_value,
         true <- Decimal.eq?(total, previous_value) do
      changeset
    else
      _ ->
        changeset
        |> put_change(target_field, total)
        |> put_change(put_if_changed, total)
    end
  end

  defp do_round(number) do
    Decimal.round(number, 2)
  end

  defp get_or_zero(record, field) do
    case Map.get(record, field) do
      nil -> Decimal.new(0)
      number -> number
    end
  end

  defp put_payoff_order(changeset) do
    debts = get_field(changeset, :debts)
    mortgages = get_field(changeset, :mortgages)
    current_payoff_order = get_field(changeset, :payoff_order)

    if (current_payoff_order == :debt_snowball and Utils.debt_snowball?(debts, mortgages)) or
         (current_payoff_order == :debt_avalanche and Utils.debt_avalanche?(debts, mortgages)) do
      changeset
    else
      payoff_order = Utils.actual_payoff_order(debts, mortgages)
      put_change(changeset, :payoff_order, payoff_order)
    end
  end
end
