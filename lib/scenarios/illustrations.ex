defmodule Scenarios.Illustrations do
  @moduledoc """
  Context for scenario illustrations
  """
  import Ecto.Query, warn: false

  alias Scenarios.{
    Illustration,
    IllustrationRow.Foresters,
    IllustrationRow.Lafayette,
    IllustrationRow.MutualTrust,
    Jobs.IllustrationProcessor,
    Parser.IllustrationParser,
    Scenario
  }

  defmodule NormalizedIllustrationYearRow do
    @moduledoc false
    defstruct [:year, :total_cash_value]
  end

  defmodule NormalizedIllustrationMonthRow do
    @moduledoc false
    defstruct [:year, :month, :cash_value]
  end

  @type source_opts :: [{:with_deleted, boolean()}]
  @spec source(source_opts) :: Ecto.Queryable.t()
  def source(opts \\ []) do
    opts
    |> Keyword.get(:with_deleted)
    |> case do
      true -> from(p in Illustration, prefix: "public")
      _ -> Illustration
    end
  end

  def schedule_process(scenario_id, upload = %Plug.Upload{}) do
    filename =
      upload.filename
      |> String.downcase()
      |> String.replace(" ", "_")

    upload = %Plug.Upload{upload | filename: filename}
    id = Ecto.UUID.generate()

    job =
      IllustrationProcessor.new(%{
        "scenario_id" => scenario_id,
        "illustration_id" => id,
        "file_name" => filename
      })

    case illustration_uploader().store({upload, id}) do
      {:ok, _path} ->
        job
        |> Oban.insert()
        |> notify(scenario_id)

      error ->
        error
    end
  end

  @doc """
  Add an illustration to a scenario
  """
  @spec create_illustration(
          Scenario.t() | String.t(),
          %{
            illustration_id: String.t(),
            illustration_parsed: IllustrationParser.t(),
            file_name: String.t()
          }
          | %{
              file_name: String.t(),
              file_content: binary(),
              illustration_id: String.t()
            }
        ) ::
          {:ok, Illustration.t()} | {:error, Ecto.Changeset.t()}
  def create_illustration(%Scenario{id: id}, data_or_file_binary),
    do: create_illustration(id, data_or_file_binary)

  def create_illustration(scenario_id, %{
        illustration_parsed: data = %IllustrationParser{},
        file_name: file_name,
        illustration_id: illustration_id
      }) do
    params =
      data
      |> Map.from_struct()
      |> Map.merge(%{scenario_id: scenario_id, file_name: file_name})

    %Illustration{id: illustration_id}
    |> Illustration.changeset(params)
    |> repo().insert()
    |> notify(scenario_id)
  end

  def create_illustration(scenario_id, %{
        illustration_id: illustration_id,
        file_name: file_name,
        pdf_data: pdf_data
      }) do
    case IllustrationParser.parse(pdf_data) do
      {:ok, data} ->
        create_illustration(scenario_id, %{
          illustration_id: illustration_id,
          illustration_parsed: data,
          file_name: file_name
        })

      err ->
        err
        |> notify(scenario_id)
    end
  end

  @doc """
  Delete an illustration from a scenario
  """
  def delete_illustration(illustration = %Illustration{}) do
    illustration
    |> Ecto.Changeset.change(%{deleted_at: DateTime.utc_now()})
    |> repo().update()
    |> notify(illustration.scenario_id)
  end

  @spec get_illustration(illustration_id :: String.t(), opts :: source_opts) ::
          Illustration.t() | nil

  @doc """
  Get an illustration
  """
  def get_illustration(illustration_id, opts \\ []) do
    opts
    |> source()
    |> repo().get(illustration_id)
  end

  def project_illustration(
        %Scenario{
          savings_account_interest_rate: savings_account_interest_rate,
          mortgages: mortgages,
          debts: debts,
          dfl_start_date: dfl_start_date
        },
        selected_illustration
      ) do
    illustration_lines = expand_illustration(selected_illustration)

    calculations_module().calculate(%{
      savings_account_interest_rate: savings_account_interest_rate,
      illustration_lines: illustration_lines,
      debts: debts,
      mortgages: mortgages,
      dfl_start_date: dfl_start_date
    })
  end

  @doc """
  Convert each parsed illustration line to a normalized line to be able to perform the calculations.

  For Foresters
  we should use
  **end_of_year** -> **year**
  **total_cash_value** -> **total_cash_value**

  For Lafayette
  we should use
  **year** -> **year**
  **net_cash_value_non_guaranteed** -> **total_cash_value**

  For MutualTrust
  we should use
  **end_of_year** -> **year**
  **cash_surrender_value_non_guaranteed** -> **total_cash_value**
  """
  def normalize_illustration_row(%Foresters{
        end_of_year: year,
        total_cash_value: cash_value
      }),
      do: %NormalizedIllustrationYearRow{
        year: year,
        total_cash_value: cash_value
      }

  def normalize_illustration_row(%Lafayette{
        year: year,
        net_cash_value_non_guaranteed: cash_value
      }),
      do: %NormalizedIllustrationYearRow{
        year: year,
        total_cash_value: cash_value
      }

  def normalize_illustration_row(%MutualTrust{
        end_of_year: year,
        cash_surrender_value_non_guaranteed: cash_value
      }),
      do: %NormalizedIllustrationYearRow{
        year: year,
        total_cash_value: cash_value
      }

  def expand_illustration(%Illustration{total_lump_sum: lump_sum, rows: rows}) do
    rows
    |> expand_illustration_rows(lump_sum, [])
    |> Enum.reverse()
  end

  defp expand_illustration_rows([], _lump_sum, rows), do: rows

  defp expand_illustration_rows(
         [row | rem],
         lump_sum,
         rows
       ) do
    %NormalizedIllustrationYearRow{year: year, total_cash_value: cash_value} =
      normalize_illustration_row(row)

    monthly = cash_value |> Decimal.sub(lump_sum) |> Decimal.div(12) |> Decimal.round(2)

    rem
    |> expand_illustration_rows(cash_value, [
      generate_month_line(year, 12, monthly, lump_sum),
      generate_month_line(year, 11, monthly, lump_sum),
      generate_month_line(year, 10, monthly, lump_sum),
      generate_month_line(year, 9, monthly, lump_sum),
      generate_month_line(year, 8, monthly, lump_sum),
      generate_month_line(year, 7, monthly, lump_sum),
      generate_month_line(year, 6, monthly, lump_sum),
      generate_month_line(year, 5, monthly, lump_sum),
      generate_month_line(year, 4, monthly, lump_sum),
      generate_month_line(year, 3, monthly, lump_sum),
      generate_month_line(year, 2, monthly, lump_sum),
      generate_month_line(year, 1, monthly, lump_sum)
      | rows
    ])
  end

  defp generate_month_line(year, month, monthly_value, lump_sum) do
    %NormalizedIllustrationMonthRow{
      year: year,
      month: month,
      cash_value: monthly_value |> Decimal.mult(month) |> Decimal.add(lump_sum)
    }
  end

  def has_scheduled_job?(%Scenario{id: id}) do
    {:ok, id} = Ecto.UUID.dump(id)

    from(j in Oban.Job,
      where:
        fragment("(?->>'scenario_id')::uuid = ?", j.args, ^id) and
          j.state not in ~w(completed discarded cancelled)
    )
    |> repo().aggregate(:count)
    |> case do
      0 -> false
      _ -> true
    end
  end

  def subscribe(scenario_id) do
    Phoenix.PubSub.subscribe(DebtFreeLife.PubSub, "#{__MODULE__}::#{scenario_id}")
  end

  defp notify({:ok, job = %Oban.Job{}}, scenario_id) do
    Phoenix.PubSub.broadcast(
      DebtFreeLife.PubSub,
      "#{__MODULE__}::#{scenario_id}",
      {:illustration_job, scenario_id}
    )

    {:ok, job}
  end

  defp notify({:ok, illustration}, scenario_id) do
    Phoenix.PubSub.broadcast(
      DebtFreeLife.PubSub,
      "#{__MODULE__}::#{scenario_id}",
      {:illustration, illustration}
    )

    {:ok, illustration}
  end

  defp notify({:error, error}, scenario_id) do
    Phoenix.PubSub.broadcast(
      DebtFreeLife.PubSub,
      "#{__MODULE__}::#{scenario_id}",
      {:invalid_illustration_job, scenario_id}
    )

    {:error, error}
  end

  defp notify(error, _scenario_id), do: error

  defp repo do
    Application.get_env(:scenarios, :setup) |> Keyword.get(:repo)
  end

  defp illustration_uploader do
    Application.get_env(:scenarios, :setup) |> Keyword.get(:illustration_uploader)
  end

  defp calculations_module do
    Application.get_env(:scenarios, :setup) |> Keyword.get(:calculations_module)
  end
end
