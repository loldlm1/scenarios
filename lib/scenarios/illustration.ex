defmodule Scenarios.Illustration do
  @moduledoc """
  Embedded schema for Scenario.Illustration
  """
  use Data, :model

  alias Scenarios.{IllustrationRow, Scenario}

  @type t() :: %__MODULE__{
          name: String.t(),
          carrier_name: String.t(),
          date: String.t(),
          name: String.t(),
          product_name: String.t(),
          total_lump_sum: integer(),
          rows: [IllustrationRow.t()],
          scenario: Scenario.t() | Ecto.Association.NotLoaded.t()
        }

  @schema_prefix "filtered"
  schema "illustrations" do
    belongs_to(:scenario, Scenario)
    field(:carrier_name, :string)
    field(:date, :string)
    field(:name, :string)
    field(:file_name, :string)
    field(:product_name, :string)
    field(:total_lump_sum, :integer, default: 0)
    field(:rows, {:array, IllustrationRow}, default: [])
    field(:deleted_at, :utc_datetime_usec)
    timestamps()
  end

  @params ~w(scenario_id carrier_name date name product_name
            total_lump_sum file_name rows deleted_at)a

  def changeset(illustration, attrs) do
    illustration
    |> Ecto.Changeset.cast(attrs, @params)
    |> Ecto.Changeset.validate_required(:scenario_id)
  end
end
