defmodule Scenarios.IncomeSource do
  @moduledoc """
  Embedded schema for Scenario.IncomeSource
  """
  use Ecto.Schema
  import Ecto.Changeset
  alias Scenarios.Utils

  defmodule Source do
    @moduledoc false
    use EctoEnum,
      wages_or_salary: "wages_or_salary",
      social_security: "social_security",
      pension: "pension",
      investment_income: "investment_income",
      rental_income: "rental_income",
      other_income: "other_income"
  end

  embedded_schema do
    field(:name, :string)
    field(:monthly_income, FormattedDecimal)
    field(:type, Source, default: :wages_or_salary)
    timestamps()
  end

  @params ~w(name monthly_income type)a
  def changeset(income_source, attrs, hard_validation \\ true) do
    income_source
    |> cast(attrs, @params)
    |> validate(hard_validation)
    |> Utils.put_action()
  end

  defp validate(changeset, true) do
    changeset
    |> Utils.force_change(@params)
    |> validate_required(@params)
    |> validate_number(:monthly_income, greater_than_or_equal_to: 0)
  end

  defp validate(changeset, false), do: changeset
end
