defmodule Scenarios.Mortgage do
  @moduledoc """
  Embedded schema for Scenario.Mortgage
  """
  use Ecto.Schema
  import Ecto.Changeset
  alias Scenarios.Utils

  defmodule Type do
    @moduledoc false
    use EctoEnum,
      fixed_term: "fixed_term",
      arm: "arm",
      interest_only: "interest_only"
  end

  embedded_schema do
    field(:interest_rate, FormattedDecimal)
    field(:monthly_payment, FormattedDecimal)
    field(:months_remaining, FormattedDecimal)
    field(:name, :string)
    field(:total_monthly_payment, FormattedDecimal)
    field(:monthly_overpayment_to_principal, FormattedDecimal)
    field(:principal, FormattedDecimal)
    field(:payoff_order, :integer)
    field(:type, Type)
  end

  @params ~w(interest_rate monthly_payment name
            monthly_overpayment_to_principal type principal payoff_order)a

  def changeset(mortgage, attrs, hard_validation \\ true) do
    mortgage
    |> cast(attrs, @params)
    |> put_months_remaining()
    |> validate(hard_validation)
    |> total_monthly_payment()
    |> Utils.put_action()
  end

  defp validate(changeset, true) do
    changeset
    |> Utils.force_change(@params)
    |> validate_required(@params)
    |> validate_number(:principal, greater_than: 0)
    |> validate_number(:interest_rate, greater_than_or_equal_to: 0, less_than_or_equal_to: 100)
    |> validate_number(:monthly_overpayment_to_principal, greater_than_or_equal_to: 0)
    |> validate_number(:monthly_payment, greater_than_or_equal_to: 0)
    |> Utils.fetch_and_validate_number(:monthly_payment,
      less_than_or_equal_to: :principal
    )
    |> validate_monthly_payment()
  end

  defp validate(changeset, false), do: changeset

  defp put_months_remaining(changeset) do
    principal = get_field(changeset, :principal)
    interest_rate = get_field(changeset, :interest_rate)
    monthly_payment = get_field(changeset, :monthly_payment)

    months_remaining =
      Utils.number_of_payments_left(%{
        principal: principal,
        interest_rate: interest_rate,
        monthly_payment: monthly_payment
      })

    put_change(changeset, :months_remaining, months_remaining)
  end

  def minimum_payment(principal, interest_rate) do
    with %Decimal{} <- principal,
         %Decimal{} <- interest_rate,
         true <- Decimal.gt?(principal, 0),
         true <- Decimal.gt?(interest_rate, 0) do
      monthly_rate = interest_rate |> Decimal.div(1200) |> Decimal.add(1)

      calculation_rate =
        monthly_rate |> Decimal.to_float() |> :math.pow(360) |> Decimal.from_float()

      principal
      |> Decimal.mult(monthly_rate |> Decimal.sub(1))
      |> Decimal.mult(calculation_rate)
      |> Decimal.div(Decimal.sub(calculation_rate, 1))
      |> Decimal.round(0, :ceiling)
    else
      _ ->
        nil
    end
  end

  defp minimum_payment(changeset) do
    principal = get_field(changeset, :principal)
    interest_rate = get_field(changeset, :interest_rate)

    minimum_payment(principal, interest_rate)
  end

  defp validate_monthly_payment(changeset) do
    if Decimal.gt?(get_field(changeset, :months_remaining), 360) do
      changeset
      |> validate_number(
        :monthly_payment,
        greater_than: minimum_payment(changeset)
      )
    else
      changeset
    end
  end

  defp total_monthly_payment(changeset) do
    total_monthly_payment =
      with monthly_payment = %Decimal{} <- get_field(changeset, :monthly_payment),
           monthly_overpayment_to_principal = %Decimal{} <-
             get_field(changeset, :monthly_overpayment_to_principal) do
        Decimal.add(monthly_payment, monthly_overpayment_to_principal)
      else
        _ -> Decimal.new(0)
      end

    put_change(changeset, :total_monthly_payment, total_monthly_payment)
  end
end
