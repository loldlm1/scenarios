defmodule Scenarios.Investment do
  @moduledoc """
  Embedded schema for Scenario.Investment
  """
  use Ecto.Schema
  import Ecto.Changeset
  alias Scenarios.Utils

  defmodule AccountType do
    @moduledoc false
    use EctoEnum,
      qualified: "qualified",
      non_qualified: "non_qualified",
      savings: "savings"
  end

  embedded_schema do
    field(:account_type, AccountType)
    field(:account_value, FormattedDecimal)
    field(:monthly_contribution, FormattedDecimal)
    field(:name, :string)
  end

  @params ~w(account_type account_value monthly_contribution name)a

  def changeset(investment, attrs, hard_validation \\ true) do
    investment
    |> cast(attrs, @params)
    |> validate(hard_validation)
    |> Utils.put_action()
  end

  defp validate(changeset, true) do
    changeset
    |> Utils.force_change(@params)
    |> validate_required(@params)
    |> validate_number(:account_value, greater_than_or_equal_to: 0)
    |> validate_number(:monthly_contribution, greater_than_or_equal_to: 0)
  end

  defp validate(changeset, false), do: changeset
end
