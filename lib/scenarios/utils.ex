defmodule Scenarios.Utils do
  @moduledoc false
  import Ecto.Changeset

  @spec fetch_and_validate_number(Ecto.Changeset.t(), atom, [
          {:less_than
           | :greater_than
           | :less_than_or_equal_to
           | :greater_than_or_equal_to
           | :equal_to
           | :not_equal_to, atom}
        ]) :: Ecto.Changeset.t()
  @doc """
    validate a field based on another one
  """
  def fetch_and_validate_number(changeset, field, opts) do
    Enum.reduce(opts, changeset, fn {type, source}, changeset ->
      case get_field(changeset, source) do
        nil -> changeset
        number -> validate_number(changeset, field, [{type, number}])
      end
    end)
  end

  def validate_interest_rate(
        changeset,
        field,
        interest_rate \\ :interest_rate,
        principal \\ :principal
      ) do
    with interest_rate_value = %Decimal{} <- Ecto.Changeset.get_field(changeset, interest_rate),
         principal_value = %Decimal{} <- Ecto.Changeset.get_field(changeset, principal) do
      minimum =
        interest_rate_value
        |> Decimal.div(100)
        |> Decimal.mult(principal_value)
        |> Decimal.div(12)
        |> Decimal.add(Decimal.mult(principal_value, Decimal.from_float(0.01)))
        |> Decimal.round(2)

      validate_number(changeset, field, greater_than_or_equal_to: minimum)
    else
      _ -> changeset
    end
  end

  def sort_debts(debts, mortgages, :custom), do: {debts, mortgages}

  def sort_debts(debts, mortgages, :debt_snowball) do
    {debts, :debt}
    |> merge_lists({mortgages, :mortgage})
    |> sort_debts_by(:principal, :asc)
  end

  def sort_debts(debts, mortgages, :debt_avalanche) do
    {debts, :debt}
    |> merge_lists({mortgages, :mortgage})
    |> sort_debts_by(:interest_rate, :desc)
  end

  defp sort_debts_by(debts, field, direction) do
    all_debts =
      debts
      |> Enum.sort_by(&sort_token(&1, field), direction)
      |> put_payoff_order()
      |> Enum.group_by(&elem(&1, 1))

    debts = Map.get(all_debts, :debt, [])
    mortages = Map.get(all_debts, :mortgage, [])
    {Enum.map(debts, &elem(&1, 0)), Enum.map(mortages, &elem(&1, 0))}
  end

  defp put_payoff_order(debts, ordered \\ [], order \\ 0)
  defp put_payoff_order([], ordered, _order), do: Enum.reverse(ordered)

  defp put_payoff_order([debt | rem], ordered, order) do
    rem
    |> put_payoff_order([do_put_payoff_order(debt, order) | ordered], order + 1)
  end

  defp do_put_payoff_order({debt = %{id: _}, type}, order),
    do: {Map.put(debt, :payoff_order, order), type}

  defp do_put_payoff_order({debt = %{}, type}, order),
    do: {Map.put(debt, "payoff_order", order), type}

  defp merge_lists({left, left_type}, {right, right_type}) do
    left
    |> to_list(left_type)
    |> Enum.concat(to_list(right, right_type))
  end

  defp to_list(source, type) do
    case source do
      source = %{} -> Map.values(source)
      source = [_ | _] -> source
      _ -> []
    end
    |> Enum.map(&{&1, type})
  end

  defp sort_token({debt, _}, field) do
    field_string = to_string(field)

    # we are adding the id in the sort to ensure consistency in the sort.
    # This will have effect on records with same target field
    debt
    |> case do
      %{:id => id, ^field => value} -> {value, id}
      %{"id" => id, ^field_string => value} -> {value, id}
      %{^field_string => value} -> {value, nil}
    end
    |> case do
      {value = %Decimal{}, id} -> {Decimal.to_float(value), id}
      {value, id} -> {value, id}
    end
  end

  def actual_payoff_order(debts, mortgages) do
    snowball = sort_debts(debts, mortgages, :debt_snowball)
    avalanche = sort_debts(debts, mortgages, :debt_avalanche)

    case {debts, mortgages} do
      ^snowball -> :debt_snowball
      ^avalanche -> :debt_avalanche
      _ -> :custom
    end
  end

  def custom?(debts, mortgages) do
    snowball = sort_debts(debts, mortgages, :debt_snowball)
    avalanche = sort_debts(debts, mortgages, :debt_avalanche)

    case {debts, mortgages} do
      ^snowball -> false
      ^avalanche -> false
      _ -> true
    end
  end

  def debt_snowball?(debts, mortgages) do
    snowball = sort_debts(debts, mortgages, :debt_snowball)

    case {debts, mortgages} do
      ^snowball -> true
      _ -> false
    end
  end

  def debt_avalanche?(debts, mortgages) do
    avalanche = sort_debts(debts, mortgages, :debt_avalanche)

    case {debts, mortgages} do
      ^avalanche -> true
      _ -> false
    end
  end

  @doc """
  Force changing a list of fields within a changeset. This is useful when validating a non-changed fields.
  """
  def force_change(changeset, fields) do
    Enum.reduce(fields, changeset, fn field, changeset ->
      value = Ecto.Changeset.get_field(changeset, field)
      Ecto.Changeset.force_change(changeset, field, value)
    end)
  end

  @doc """
  Put ecto changeset action to the changeset.
  This is required to show which fields has any error
  """
  def put_action(changeset = %Ecto.Changeset{data: %{id: id}}) when is_binary(id),
    do: Map.put(changeset, :action, :update)

  def put_action(changeset), do: Map.put(changeset, :action, :insert)
  def describe_period(0), do: nil
  def describe_period(1), do: "1 Month"

  def describe_period(months) when months < 12 do
    "#{months} Months"
  end

  def describe_period(12), do: "1 Year"

  def describe_period(months) when rem(months, 12) == 0 do
    "#{floor(months / 12)} Years"
  end

  def describe_period(months) do
    rem = rem(months, 12)
    describe_period(months - rem) <> " and " <> describe_period(rem)
  end

  def number_of_payments_left(
        debt = %{
          interest_rate: interest_rate,
          principal: principal,
          monthly_payment: monthly_payment
        }
      ) do
    cond do
      is_nil(monthly_payment) or is_nil(principal) ->
        Decimal.new(0)

      Decimal.eq?(monthly_payment, 0) or Decimal.eq?(principal, 0) ->
        Decimal.new(0)

      is_nil(interest_rate) or Decimal.eq?(interest_rate, 0) ->
        principal
        |> Decimal.div(monthly_payment)
        |> Decimal.round()

      true ->
        do_calculate_number_of_payments_left(debt)
    end
  end

  defp do_calculate_number_of_payments_left(%{
         principal: principal,
         interest_rate: interest_rate,
         monthly_payment: monthly_payment
       }) do
    interest_rate = Decimal.div(interest_rate, 100)
    number_of_payments_per_year = 12

    %{mid: mid} =
      Enum.reduce_while(
        0..9,
        %{
          error_mid: Decimal.new(0),
          low: Decimal.new(0),
          mid: Decimal.new(0),
          high: Decimal.new(1024),
          interest_rate: interest_rate,
          principal: principal,
          monthly_payment: monthly_payment,
          number_of_payments_per_year: number_of_payments_per_year
        },
        &do_calculate_number_of_payments_left/2
      )

    mid
  end

  defp do_calculate_number_of_payments_left(
         _count,
         params = %{
           high: high,
           low: low,
           interest_rate: interest_rate,
           principal: principal,
           monthly_payment: monthly_payment,
           number_of_payments_per_year: number_of_payments_per_year
         }
       ) do
    mid = high |> Decimal.add(low) |> Decimal.div(2)

    error_mid =
      calculate_number_of_payments_error(
        principal,
        interest_rate,
        mid,
        number_of_payments_per_year,
        monthly_payment
      )

    cond do
      Decimal.gt?(error_mid, 0) ->
        {:cont, %{params | low: mid, mid: mid, error_mid: error_mid}}

      Decimal.lt?(error_mid, 0) ->
        {:cont, %{params | high: mid, mid: mid, error_mid: error_mid}}

      Decimal.eq?(error_mid, 0) ->
        {:halt, %{params | mid: mid, error_mid: error_mid}}
    end
  end

  defp calculate_number_of_payments_error(
         principal,
         interest_rate,
         result,
         number_of_payments_per_year,
         monthly_payment
       ) do
    result1 = Decimal.div(interest_rate, number_of_payments_per_year)
    result2 = binomial(result1, Decimal.mult(result, -1))

    1
    |> Decimal.sub(result2)
    |> Decimal.div(result1)
    |> Decimal.mult(monthly_payment)
    |> Decimal.mult(-1)
    |> Decimal.add(principal)
  end

  defp binomial(step, %{rate: rate, cof: cof, pow: pow, sum: sum, term: term}) do
    cof = cof |> Decimal.mult(pow) |> Decimal.div(step)
    pow = Decimal.sub(pow, 1)
    term = Decimal.mult(term, rate)
    sum = cof |> Decimal.mult(term) |> Decimal.add(sum)

    %{
      rate: rate,
      cof: cof,
      pow: pow,
      sum: sum,
      term: term
    }
  end

  defp binomial(rate, mid) do
    if Decimal.lt?(mid, 0) do
      Decimal.div(1, binomial(rate, Decimal.mult(mid, -1)))
    else
      sum = Decimal.new(1)
      pow = Decimal.new(mid)
      term = 1
      cof = 1

      %{sum: sum} =
        Enum.reduce(
          1..9,
          %{
            rate: rate,
            cof: cof,
            pow: pow,
            sum: sum,
            term: term
          },
          &binomial/2
        )

      sum
    end
  end
end
