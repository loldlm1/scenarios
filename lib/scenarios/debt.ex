defmodule Scenarios.Debt do
  @moduledoc """
  Embedded schema for Scenario.Debt
  """

  use Ecto.Schema
  import Ecto.Changeset
  alias Scenarios.Utils

  defmodule Type do
    @moduledoc false
    use EctoEnum, revolving: "revolving", installment: "installment"
  end

  embedded_schema do
    field(:interest_rate, FormattedDecimal)
    field(:introductory_interest_rate, FormattedDecimal)
    field(:with_introductory, :boolean)
    field(:overrage, FormattedDecimal)
    field(:minimum_monthly_payment, FormattedDecimal)
    field(:actual_monthly_payment, FormattedDecimal)
    field(:name, :string)
    field(:payoff_order, :integer)
    field(:principal, FormattedDecimal)
    field(:transition_date, :date)
    field(:type, Type, default: :revolving)
    field(:months_remaining, FormattedDecimal)
  end

  @params ~w(interest_rate minimum_monthly_payment actual_monthly_payment name
           payoff_order principal transition_date introductory_interest_rate type with_introductory)a
  @required ~w(interest_rate minimum_monthly_payment actual_monthly_payment name
           principal type)a

  def changeset(debt, attrs, hard_validation \\ true) do
    debt
    |> cast(attrs, @params)
    |> check_introductory_interest_rate()
    |> put_months_remaining()
    |> validate(hard_validation)
    |> calculate_overrage()
    |> Utils.put_action()
  end

  defp validate(changeset, true) do
    changeset
    |> Utils.force_change(@params)
    |> validate_required(@required)
    |> validate_number(:principal, greater_than: 0)
    |> validate_number(:interest_rate, greater_than_or_equal_to: 0, less_than_or_equal_to: 100)
    |> validate_number(:introductory_interest_rate,
      greater_than_or_equal_to: 0,
      less_than_or_equal_to: 100
    )
    |> validate_number(:minimum_monthly_payment, greater_than: 0)
    |> Utils.fetch_and_validate_number(:minimum_monthly_payment,
      less_than_or_equal_to: :principal
    )
    |> Utils.fetch_and_validate_number(:actual_monthly_payment,
      less_than_or_equal_to: :principal
    )
    |> Utils.fetch_and_validate_number(:actual_monthly_payment,
      greater_than_or_equal_to: :minimum_monthly_payment
    )
    |> validate_rate(get_field(changeset, :type))
  end

  defp validate(changeset, false), do: changeset

  defp validate_rate(changeset, :revolving) do
    changeset
    |> Utils.validate_interest_rate(:minimum_monthly_payment)
    |> Utils.validate_interest_rate(:actual_monthly_payment)
  end

  defp validate_rate(changeset, _) do
    changeset
    |> validate_number(:minimum_monthly_payment, greater_than: 0)
    |> validate_number(:actual_monthly_payment, greater_than: 0)
  end

  defp calculate_overrage(changeset) do
    minimum_monthly_payment = get_field(changeset, :minimum_monthly_payment) || 0
    actual_monthly_payment = get_field(changeset, :actual_monthly_payment) || 0

    put_change(
      changeset,
      :overrage,
      actual_monthly_payment
      |> Decimal.sub(minimum_monthly_payment)
      |> Decimal.max(Decimal.from_float(0.0))
    )
  end

  defp put_months_remaining(changeset) do
    principal = get_field(changeset, :principal)
    interest_rate = get_field(changeset, :interest_rate)
    minimum_monthly_payment = get_field(changeset, :minimum_monthly_payment)

    months_remaining =
      Utils.number_of_payments_left(%{
        principal: principal,
        interest_rate: interest_rate,
        monthly_payment: minimum_monthly_payment
      })

    put_change(changeset, :months_remaining, months_remaining)
  end

  defp check_introductory_interest_rate(changeset) do
    case get_field(changeset, :with_introductory) do
      true ->
        changeset

      _ ->
        changeset
        |> put_change(:introductory_interest_rate, nil)
        |> put_change(:transition_date, nil)
    end
  end
end
