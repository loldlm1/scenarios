defmodule Scenarios.Repo do
  use Ecto.Repo,
    otp_app: :scenarios,
    adapter: Ecto.Adapters.Postgres

  require Logger

  @doc """
  Dynamically loads the repository url from the
  """
  def init(_, opts) do
    {:ok, Keyword.merge(opts, [])}
  end
end
