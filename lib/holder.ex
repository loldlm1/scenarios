defmodule Scenarios.Holder do
  @moduledoc """
  Holder schema
  """
  @holder_name Application.get_env(:scenarios, :setup) |> Keyword.get(:holder_name)
  use Data, :model

  @type t() :: %__MODULE__{
          id: String.t()
        }

  schema "#{@holder_name}s" do
    field(:submitted_to_carrier, :boolean, default: false)
    field(:date_of_birth, :date)
    field(:spouse_date_of_birth, :date)
    field(:full_address, {:array, :map})
    field(:address, :string)
    field(:city, :string)
    field(:zip, :string)
    field(:first_name, :string)
    field(:last_name, :string)
    has_many(:scenarios, Scenarios.Scenario, foreign_key: :"#{@holder_name}_id")
    timestamps()
  end

  @required_params ~w()a
  @optional_params ~w(submitted_to_carrier date_of_birth spouse_date_of_birth full_address address city zip first_name last_name)a

  def changeset(holder, attrs \\ %{}) do
    holder
    |> cast(attrs, cast_params(holder))
    |> validate_required(@required_params)
  end

  defp cast_params(_), do: @required_params ++ @optional_params
end

defmodule Scenarios.Holders do
  def get_holder(id) do
    Scenarios.Repo.get(Scenarios.Holder, id)
  end
end
