defmodule Scenarios.Parser.IllustrationParser do
  @moduledoc """
  Illustration PDF parser logic
  """
  defstruct [:name, :product_name, :carrier_name, :date, :total_lump_sum, :rows]

  def parse(file_data) do
    with {:ok, type} <- identify_pdf(file_data) do
      parse(file_data, type)
    end
  end

  def identify_pdf(file_data) do
    file_data
    |> identify_type()
    |> parse_identify_response()
  rescue
    _ -> {:error, "Could not read malformed PDF file"}
  end

  def parse(file_data, type) do
    file_data
    |> extract(type)
    |> parse_response()
  end

  defp identify_type(file_data), do: run_script_on_pdf_file(identification_script(), file_data)

  def extract(file_data, type), do: run_script_on_pdf_file(extract_script(type), file_data)

  def run_script_on_pdf_file(script, file_data) do
    with {:ok, path} <- write_temp_pdf(file_data) do
      MuonTrap.cmd("python3", [script, path])
    end
  end

  defp identification_script, do: script_path("identify.py")

  defp extract_script(:foresters), do: script_path("foresters.py")
  defp extract_script(:lafayette), do: script_path("lafayette.py")
  defp extract_script(:mutual_trust), do: script_path("mutual_trust.py")

  def script_path(filename), do: Path.join(~w(#{priv_dir()} illustration_parser #{filename}))

  defp priv_dir, do: :code.priv_dir(:scenarios)

  defp parse_identify_response({bert_data, 0}) do
    bert_data
    |> parse_bert()
  end

  defp parse_identify_response({"", 1}) do
    parse_bert("")
  end

  defp parse_response({bert_data, 0}) do
    response =
      bert_data
      |> parse_bert()

    with {total_lump_sum, _} <- parse_integer(response['total_lump_sum']),
         product <- String.Chars.to_string(response['product_name']),
         name <- String.Chars.to_string(response['carrier_name']),
         date <- String.Chars.to_string(response['date']) do
      rows = response['tabular_details']

      illustration = %__MODULE__{
        name: "untitled",
        product_name: product,
        carrier_name: name,
        date: date,
        total_lump_sum: total_lump_sum,
        rows: rows
      }

      {:ok, illustration}
    end
  end

  defp parse_response({bytes, _}) do
    parse_bert(bytes)
  end

  defp parse_response(_), do: :error

  defp parse_bert(bytes) when byte_size(bytes) == 0,
    do: {:error, "Could not read malformed PDF file"}

  defp parse_bert(bytes) do
    case :erlang.binary_to_term(bytes) do
      {:ok, type} ->
        {:ok, type}

      %{'error' => error} ->
        {:error, "#{error}"}

      {:error, error} ->
        {:error, "#{error}"}

      data ->
        data
    end
  end

  # defp parse_decimal(:undefined), do: nil
  # defp parse_decimal(i) when is_integer(i), do: Decimal.new(i)
  # defp parse_decimal(i) when is_float(i), do: Decimal.from_float(i)

  defp parse_integer(i) when is_integer(i), do: {i, ""}
  defp parse_integer(i), do: Integer.parse("#{i}")

  def write_temp_pdf(file_data) do
    with {:ok, path} <- Briefly.create(extname: ".pdf"),
         :ok <- File.write(path, file_data) do
      {:ok, path}
    end
  end
end
