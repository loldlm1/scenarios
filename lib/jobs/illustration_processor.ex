defmodule Scenarios.Jobs.IllustrationProcessor do
  @moduledoc """
  A job to parse an uploaded PDF file for a scenario
  """
  use Oban.Worker, queue: :illustrations
  alias Scenarios.Illustrations
  @impl Oban.Worker
  def perform(%Oban.Job{
        args: %{
          "illustration_id" => illustration_id,
          "scenario_id" => scenario_id,
          "file_name" => file_name
        }
      }) do
    with url <-
           illustration_uploader().url({file_name, illustration_id}, signed: true),
         {:ok, pdf_data} <- download_file(url) do
      Illustrations.create_illustration(scenario_id, %{
        file_name: file_name,
        pdf_data: pdf_data,
        illustration_id: illustration_id
      })
      |> case do
        {:ok, result} -> {:ok, result}
        {:error, error} when is_binary(error) -> :ok
        error -> error
      end
    end
  end

  defp download_file(url = "http" <> _) do
    with {:ok, %HTTPoison.Response{body: body}} <- HTTPoison.get(url) do
      {:ok, body}
    end
  end

  # for testinging
  defp download_file(path = "/http://" <> _) do
    "/" <> path = path
    download_file(path)
  end

  defp download_file(path) do
    File.read(path)
  end

  defp illustration_uploader do
    Application.get_env(:scenarios, :setup) |> Keyword.get(:illustration_uploader)
  end
end
