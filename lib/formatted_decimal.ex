defmodule FormattedDecimal do
  @moduledoc """
  Decimal type to parse formateed numbers
  """
  use Ecto.Type

  def type, do: :decimal

  def cast(decimal = %Decimal{}), do: {:ok, decimal}

  def cast(binary) when is_binary(binary) do
    binary
    |> String.replace(",", "")
    |> Decimal.parse()
  end

  def cast(float) when is_float(float),
    do: {:ok, Decimal.from_float(float)}

  def cast(int) when is_integer(int),
    do: {:ok, Decimal.new(int)}

  def load(decimal), do: {:ok, decimal}
  def dump(decimal), do: {:ok, decimal}
end
