defmodule Scenarios.Migration do
  defmacro __using__(_) do
    quote do
      use Ecto.Migration
      @holder_name Application.get_env(:scenarios, :setup) |> Keyword.get(:holder_name)

      def change do
        create_if_not_exists table(:"#{@holder_name}s", primary_key: false) do
          add(:id, :uuid, null: false, primary_key: true)
          add(:submitted_to_carrier, :boolean, default: false)
          add(:date_of_birth, :date)
          add(:spouse_date_of_birth, :date)
          add(:full_address, :tsvector)
          add(:address, :string)
          add(:city, :string)
          add(:zip, :string)
          add(:state, :string)
          add(:first_name, :string)
          add(:last_name, :string)
          timestamps()
        end

        create(index("#{@holder_name}s", ["full_address"]))

        execute("create schema filtered", "drop schema filtered")
        generate_filtered_view("#{@holder_name}s", initial: true)

        create table(:scenarios, primary_key: false) do
          add(:id, :uuid, null: false, primary_key: true)
          add(:name, :string)
          add(:goals, :text)
          add(:savings_account_interest_rate, :float)
          add(:payoff_order, :string)
          add(:submitted_to_carrier, :boolean, default: false, null: false)
          add(:income_sources, {:array, :map}, default: [])
          add(:insurances, {:array, :map}, default: [])
          add(:investments, {:array, :map}, default: [])
          add(:mortgages, {:array, :map}, default: [])
          add(:debts, {:array, :map}, default: [])
          add(:is_valid, :boolean, default: true)
          add(:dfl_start_date, :date)

          add(:total_debt_overpayment, :float, default: 0)
          add(:debt_overpayment_not_available, :float, default: 0)
          add(:debt_overpayment_available, :float, default: 0)
          add(:total_insurance_monthly_premium_redirecting, :float, default: 0)
          add(:insurance_monthly_premium_redirecting_not_available, :float, default: 0)
          add(:insurance_monthly_premium_redirecting_available, :float, default: 0)
          add(:total_insurance_cash_value_redirecting, :float, default: 0)
          add(:insurance_cash_value_redirecting_not_available, :float, default: 0)
          add(:insurance_cash_value_redirecting_available, :float, default: 0)
          add(:total_investments_monthly_premium_redirecting, :float, default: 0)
          add(:investments_monthly_premium_redirecting_not_available, :float, default: 0)
          add(:investments_monthly_premium_redirecting_available, :float, default: 0)
          add(:total_investments_cash_value_redirecting, :float, default: 0)
          add(:investments_cash_value_redirecting_not_available, :float, default: 0)
          add(:investments_cash_value_redirecting_available, :float, default: 0)
          add(:mortgage_overpayment_redirecting_available, :decimal, default: 0)
          add(:total_mortgage_overpayment_redirecting, :decimal, default: 0)
          add(:income_redirecting_available, :decimal, default: 0)
          add(:total_income_redirecting, :decimal, default: 0)

          add(:"#{@holder_name}_id", references(:"#{@holder_name}s", type: :binary_id, on_delete: :delete_all))
          timestamps()
        end

        create(index(:scenarios, [:"#{@holder_name}_id"]))

        generate_filtered_view("scenarios", initial: true)

        create table(:illustrations, primary_key: false) do
          add(:id, :uuid, null: false, primary_key: true)
          add(:scenario_id, references(:scenarios, type: :uuid))
          add(:rows, {:map, :array})
          add(:carrier_name, :string)
          add(:date, :string)
          add(:name, :string)
          add(:product_name, :string)
          add(:total_lump_sum, :integer, default: 0)
          add(:file_name, :string)
          timestamps()
        end

        create(index(:illustrations, [:scenario_id]))
        generate_filtered_view(:illustrations, initial: true)

        generate_filtered_view(:scenarios) do
          alter table(:scenarios) do
            remove(:debt_overpayment_not_available, :float)
            remove(:insurance_monthly_premium_redirecting_not_available, :float)
            remove(:insurance_cash_value_redirecting_not_available, :float)
            remove(:investments_monthly_premium_redirecting_not_available, :float)
            remove(:investments_cash_value_redirecting_not_available, :float)
          end

          modify_column(:savings_account_interest_rate)
          modify_column(:total_debt_overpayment)
          modify_column(:debt_overpayment_available)
          modify_column(:total_insurance_monthly_premium_redirecting)
          modify_column(:insurance_monthly_premium_redirecting_available)
          modify_column(:total_insurance_cash_value_redirecting)
          modify_column(:insurance_cash_value_redirecting_available)
          modify_column(:total_investments_monthly_premium_redirecting)
          modify_column(:investments_monthly_premium_redirecting_available)
          modify_column(:total_investments_cash_value_redirecting)
          modify_column(:investments_cash_value_redirecting_available)
        end

        create(
          index(:scenarios, [:"#{@holder_name}_id", :submitted_to_carrier],
            where: "submitted_to_carrier is true"
          )
        )

        execute(
          """
            do $$
              begin
              create function full_address_trigger() returns trigger as $T$
                begin
                  new.full_address = to_tsvector(new."address" || ' ' || new."city" || ' ' || new."state" || ' ' || new."zip");
                  return new;
                end
              $T$ language plpgsql;
              create trigger full_address before insert or update on #{@holder_name}s
              for each row execute procedure full_address_trigger();
              end
            $$;
          """,
          """
          do $$
            begin
              drop trigger full_address on #{@holder_name}s;
              drop function full_address_trigger();
            end
          $$
          """
        )

        execute("update #{@holder_name}s set address = address", "")
      end

      defp modify_column(column) do
        execute(modify_column_query(column, "decimal"), modify_column_query(column, "float"))
      end

      defp modify_column_query(column, type) do
        "alter table scenarios alter column #{column} type #{type}"
      end
    end
  end


  @moduledoc """
  Helper function for database migration
  """
  @spec generate_filtered_view(String.t() | atom, [{:initial, boolean}]) :: any
  @doc """
  Generates first filtered view for the first time

  ```elixir
  def change do
    generate_filtered_view TABLE_NAME, initial: true
  end
  ```

  to update the table after generating the view
  ```elixir
  def change do
    generate_filtered_view(TABLE_NAME) do
      alter table(TABLE_NAME) do
    #... any migration goes here
      end
    end
  end
  ```
  """
  defmacro generate_filtered_view(table_name, opts \\ []) do
    block = Keyword.get(opts, :do)
    initial = Keyword.get(opts, :initial, false)

    quote location: :keep do
      table_name = unquote(table_name)

      up = """
       create or replace view filtered.#{table_name} as (
        select * from public.#{table_name} where deleted_at is null
      )
      """

      down = """
        drop view if exists filtered.#{table_name}
      """

      if direction() == :up do
        if unquote(initial) do
          alter table(table_name) do
            add_if_not_exists(:deleted_at, :utc_datetime)
          end

          create_if_not_exists(index(table_name, :deleted_at))
        end

        execute(down, "")
        unquote(block)
        execute(up, "")
      else
        unless unquote(initial) do
          execute("", up)
        end

        unquote(block)
        execute("", down)
      end
    end
  end
end
