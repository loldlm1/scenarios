defmodule Scenarios.Repo do
  @moduledoc false
  use Ecto.Repo,
    otp_app: :scenarios,
    adapter: Ecto.Adapters.Postgres
end