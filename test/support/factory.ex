defmodule Scenarios.RepoFactory do
  @moduledoc """
  The factory to mock data for test purposes.
  """
  @holder_name Application.get_env(:scenarios, :setup) |> Keyword.get(:holder_name)
  use ExMachina.Ecto, repo: Scenarios.Repo

  def holder_factory do
    %Scenarios.Holder{
      submitted_to_carrier: false,
      first_name: Faker.Person.first_name(),
      last_name: Faker.Person.last_name()
    }
  end

  def scenario_factory do
    %Scenarios.Scenario{
      @holder_name => build(:holder),
      :name => Faker.Person.first_name() <> " " <> Faker.Person.last_name()
    }
  end

  def illustration_factory do
    %Scenarios.Illustration{
      scenario: build(:scenario),
      rows: [
        %Scenarios.IllustrationRow.Foresters{
          accum_paid_up_add: Decimal.new(2397),
          age: Decimal.new(71),
          annual_dividend: Decimal.new(1335),
          contract_premium: Decimal.new(201_925),
          end_of_year: Decimal.new(1),
          guar_cash_value: Decimal.new(157_854),
          guar_death_benefit: Decimal.new(1_195_713),
          paid_up_cash_value: Decimal.new(1335),
          premium_outlay: Decimal.new(201_925),
          total_cash_value: Decimal.new(159_189),
          total_death_benefit: Decimal.new(1_198_110)
        },
        %Scenarios.IllustrationRow.Foresters{
          accum_paid_up_add: Decimal.new(7123),
          age: Decimal.new(72),
          annual_dividend: Decimal.new(2687),
          contract_premium: Decimal.new(93_960),
          end_of_year: Decimal.new(2),
          guar_cash_value: Decimal.new(217_896),
          guar_death_benefit: Decimal.new(1_288_890),
          paid_up_cash_value: Decimal.new(4082),
          premium_outlay: Decimal.new(93_960),
          total_cash_value: Decimal.new(221_978),
          total_death_benefit: Decimal.new(1_296_013)
        },
        %Scenarios.IllustrationRow.Foresters{
          accum_paid_up_add: Decimal.new(12_864),
          age: Decimal.new(73),
          annual_dividend: Decimal.new(3318),
          contract_premium: Decimal.new(93_960),
          end_of_year: Decimal.new(3),
          guar_cash_value: Decimal.new(281_312),
          guar_death_benefit: Decimal.new(1_379_497),
          paid_up_cash_value: Decimal.new(7578),
          premium_outlay: Decimal.new(93_960),
          total_cash_value: Decimal.new(288_890),
          total_death_benefit: Decimal.new(1_392_362)
        },
        %Scenarios.IllustrationRow.Foresters{
          accum_paid_up_add: Decimal.new(19_656),
          age: Decimal.new(74),
          annual_dividend: Decimal.new(3992),
          contract_premium: Decimal.new(93_960),
          end_of_year: Decimal.new(4),
          guar_cash_value: Decimal.new(346_195),
          guar_death_benefit: Decimal.new(1_467_663),
          paid_up_cash_value: Decimal.new(11_894),
          premium_outlay: Decimal.new(93_960),
          total_cash_value: Decimal.new(358_090),
          total_death_benefit: Decimal.new(1_487_319)
        }
      ]
    }
  end

  def debts_stream do
    StreamData.fixed_map(%{
      principal: StreamData.integer(1_000..50_000),
      payoff_order: StreamData.integer(0..100_000),
      months: StreamData.integer(20..180),
      speedup: StreamData.integer(0..5),
      interest_rate: StreamData.float(min: 0.01, max: 0.50)
    })
    |> StreamData.bind(fn %{
                            principal: principal,
                            payoff_order: payoff_order,
                            months: months,
                            speedup: speedup,
                            interest_rate: interest_rate
                          } ->
      rate =
        :math.pow(1 + interest_rate, months / 12)
        |> Decimal.from_float()

      %{
        name: Faker.Company.name(),
        actual_monthly_payment:
          principal
          |> Decimal.new()
          |> Decimal.mult(rate)
          |> Decimal.div(months - speedup)
          |> Decimal.round(2),
        minimum_monthly_payment:
          principal
          |> Decimal.new()
          |> Decimal.mult(rate)
          |> Decimal.div(months)
          |> Decimal.round(2),
        principal: Decimal.new(principal) |> Decimal.round(),
        interest_rate: Decimal.from_float(interest_rate * 100) |> Decimal.round(2),
        payoff_order: payoff_order
      }
      |> StreamData.constant()
    end)
  end

  def mortgages_stream do
    StreamData.fixed_map(%{
      principal: StreamData.integer(100_000..500_000),
      payoff_order: StreamData.integer(0..100_000),
      speedup: StreamData.integer(0..5),
      months: StreamData.integer(20..360),
      interest_rate: StreamData.float(min: 0.01, max: 0.07)
    })
    |> StreamData.bind(fn %{
                            principal: principal,
                            payoff_order: payoff_order,
                            speedup: speedup,
                            months: months,
                            interest_rate: interest_rate
                          } ->
      rate =
        :math.pow(1 + interest_rate, months / 12)
        |> Decimal.from_float()

      %{
        name: Faker.Company.name(),
        total_monthly_payment:
          principal
          |> Decimal.new()
          |> Decimal.mult(rate)
          |> Decimal.div(months - speedup)
          |> Decimal.round(2),
        monthly_payment:
          principal
          |> Decimal.new()
          |> Decimal.mult(rate)
          |> Decimal.div(months)
          |> Decimal.round(2),
        principal: Decimal.new(principal),
        interest_rate: Decimal.from_float(interest_rate * 100) |> Decimal.round(2),
        payoff_order: payoff_order,
        months_remaining: months
      }
      |> StreamData.constant()
    end)
  end

  def income_sources_stream do
    StreamData.bind(StreamData.integer(10..50_000), fn monthly_income ->
      StreamData.bind(
        StreamData.member_of([
          :wages_or_salary,
          :social_security,
          :pension,
          :investment_income,
          :rental_income,
          :other_income
        ]),
        fn type ->
          %{
            monthly_income: Decimal.new(monthly_income),
            name: Faker.Company.name(),
            type: type
          }
          |> StreamData.constant()
        end
      )
    end)
  end

  def insurances_stream do
    StreamData.fixed_map(%{
      monthly_premium: StreamData.integer(10..5_000),
      cash_value: StreamData.integer(10_000..50_000),
      death_benefit: StreamData.integer(100_000..500_000),
      type: StreamData.member_of([:permanent, :term]),
      rating: StreamData.member_of([:standard, :standard_non_tobacco, :tobacco])
    })
    |> StreamData.bind(fn %{
                            monthly_premium: monthly_premium,
                            cash_value: cash_value,
                            death_benefit: death_benefit,
                            type: type,
                            rating: rating
                          } ->
      %{
        monthly_premium: Decimal.new(monthly_premium),
        name: Faker.Company.name(),
        cash_value: cash_value,
        death_benefit: death_benefit,
        rating: rating,
        type: type
      }
      |> StreamData.constant()
    end)
  end

  def investments_stream do
    StreamData.fixed_map(%{
      monthly_contribution: StreamData.integer(10..5_000),
      account_value: StreamData.integer(10_000..50_000),
      account_type: StreamData.member_of([:qualified, :non_qualified, :savings])
    })
    |> StreamData.bind(fn %{
                            monthly_contribution: monthly_contribution,
                            account_value: account_value,
                            account_type: account_type
                          } ->
      %{
        monthly_contribution: Decimal.new(monthly_contribution),
        name: Faker.Company.name(),
        account_value: Decimal.new(account_value),
        account_type: account_type
      }
      |> StreamData.constant()
    end)
  end

  def scenario_stream do
    StreamData.fixed_map(%{
      debts: debts_stream() |> list_of(),
      mortgages: mortgages_stream() |> list_of(),
      investments: investments_stream() |> list_of(),
      income_sources: income_sources_stream() |> list_of(),
      insurances: insurances_stream() |> list_of(),
      savings_account_interest_rate: StreamData.integer(0..20)
    })
    |> StreamData.bind(fn params ->
      %{
        changes: %{
          total_debt_overpayment: total_debt_overpayment,
          total_insurance_monthly_premium_redirecting:
            total_insurance_monthly_premium_redirecting,
          total_insurance_cash_value_redirecting: total_insurance_cash_value_redirecting,
          total_investments_monthly_premium_redirecting:
            total_investments_monthly_premium_redirecting,
          total_investments_cash_value_redirecting: total_investments_cash_value_redirecting,
          debt_overpayment_available: debt_overpayment_available,
          insurance_monthly_premium_redirecting_available:
            insurance_monthly_premium_redirecting_available,
          insurance_cash_value_redirecting_available: insurance_cash_value_redirecting_available,
          investments_monthly_premium_redirecting_available:
            investments_monthly_premium_redirecting_available,
          investments_cash_value_redirecting_available:
            investments_cash_value_redirecting_available
        }
      } = Scenarios.Scenarios.change_scenario(params)

      params
      |> Map.merge(%{
        total_debt_overpayment: total_debt_overpayment,
        total_insurance_monthly_premium_redirecting: total_insurance_monthly_premium_redirecting,
        total_insurance_cash_value_redirecting: total_insurance_cash_value_redirecting,
        total_investments_monthly_premium_redirecting:
          total_investments_monthly_premium_redirecting,
        total_investments_cash_value_redirecting: total_investments_cash_value_redirecting,
        debt_overpayment_available: debt_overpayment_available,
        insurance_monthly_premium_redirecting_available:
          insurance_monthly_premium_redirecting_available,
        insurance_cash_value_redirecting_available: insurance_cash_value_redirecting_available,
        investments_monthly_premium_redirecting_available:
          investments_monthly_premium_redirecting_available,
        investments_cash_value_redirecting_available: investments_cash_value_redirecting_available
      })
      |> StreamData.constant()
    end)
  end

  defp list_of(source, max \\ 5) do
    StreamData.list_of(source, min_length: 2, max_length: max)
  end
end
