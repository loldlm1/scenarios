defmodule Scenarios.ScenariosTotalTest do
  use Scenarios.DataCase, asycn: true

  alias Scenarios.{
    Insurance,
    Investment,
    Mortgage,
    Scenario,
    Total
  }

  describe "build/1" do
    test "should add lines from investments" do
      scenario = %Scenario{
        investments: [
          %Investment{
            name: "name",
            monthly_contribution: 10,
            account_value: 500
          }
        ]
      }

      assert %Total{
               scenario: ^scenario,
               lines: [
                 %Total.Line{
                   account: "name",
                   monthly_contribution: 10,
                   total_value: 500
                 }
               ]
             } = Total.build(scenario)
    end

    test "should add lines from mortgages" do
      scenario = %Scenario{
        mortgages: [
          %Mortgage{
            name: "name",
            monthly_payment: 50,
            principal: 50_000
          }
        ]
      }

      assert %Total{
               scenario: ^scenario,
               lines: [
                 %Total.Line{
                   account: "name",
                   monthly_contribution: 50,
                   total_value: 50_000
                 }
               ]
             } = Total.build(scenario)
    end

    test "should add lines from insurances" do
      scenario = %Scenario{
        insurances: [
          %Insurance{
            name: "insurance 1",
            monthly_premium: 120,
            cash_value: 500
          }
        ]
      }

      assert %Total{
               scenario: ^scenario,
               lines: [
                 %Total.Line{
                   account: "insurance 1",
                   monthly_contribution: 120,
                   total_value: 500
                 }
               ]
             } = Total.build(scenario)
    end

    test "should have correct order investments then mortgages then insurances" do
      scenario = %Scenario{
        investments: [
          %Investment{
            name: "investment 1",
            monthly_contribution: 10,
            account_value: 500
          },
          %Investment{
            name: "investment 2",
            monthly_contribution: 20,
            account_value: 1000
          }
        ],
        mortgages: [
          %Mortgage{
            name: "mortgage 1",
            monthly_payment: 50,
            principal: 5000
          },
          %Mortgage{
            name: "mortgage 2",
            monthly_payment: 100,
            principal: 10_000
          }
        ],
        insurances: [
          %Insurance{
            name: "Insurance 1",
            monthly_premium: 120,
            cash_value: 500
          },
          %Insurance{
            name: "Insurance 2",
            monthly_premium: 240,
            cash_value: 1000
          }
        ]
      }

      assert %Total{
               scenario: ^scenario,
               lines: [
                 %Total.Line{
                   account: "investment 1",
                   monthly_contribution: 10,
                   total_value: 500
                 },
                 %Total.Line{
                   account: "investment 2",
                   monthly_contribution: 20,
                   total_value: 1000
                 },
                 %Total.Line{
                   account: "mortgage 1",
                   monthly_contribution: 50,
                   total_value: 5000
                 },
                 %Total.Line{
                   account: "mortgage 2",
                   monthly_contribution: 100,
                   total_value: 10_000
                 },
                 %Total.Line{
                   account: "Insurance 1",
                   monthly_contribution: 120,
                   total_value: 500
                 },
                 %Total.Line{
                   account: "Insurance 2",
                   monthly_contribution: 240,
                   total_value: 1000
                 }
               ]
             } = Total.build(scenario)
    end

    test "should calculate generate summary for lines" do
      scenario = %Scenario{
        investments: [
          %Investment{
            name: "investment 1",
            monthly_contribution: 10,
            account_value: 500
          },
          %Investment{
            name: "investment 2",
            monthly_contribution: 20,
            account_value: 1000
          }
        ],
        mortgages: [
          %Mortgage{
            name: "mortgage 1",
            monthly_payment: 50
          },
          %Mortgage{
            name: "mortgage 2",
            monthly_payment: 100
          }
        ],
        insurances: [
          %Insurance{
            name: "Insurance 1",
            monthly_premium: 120,
            cash_value: 500
          },
          %Insurance{
            name: "Insurance 2",
            monthly_premium: 240,
            cash_value: 1000
          }
        ]
      }

      assert %Total{
               scenario: ^scenario,
               summary: %Total.Summary{}
             } = Total.build(scenario)
    end
  end
end
