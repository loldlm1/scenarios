defmodule Scenarios.ScenariosTest do
  @holder_name Application.get_env(:scenarios, :setup) |> Keyword.get(:holder_name)
  use Scenarios.DataCase, asycn: true

  alias Scenarios.{
    Debt,
    IncomeSource,
    Insurance,
    Investment,
    Mortgage,
    Holders,
    Scenarios,
    Scenario
  }

  defmacrop quoted_decimal(decimal) do
    decimal =
      decimal
      |> case do
        float when is_float(float) -> Decimal.from_float(float)
        integer -> Decimal.new(integer)
      end
      |> Macro.escape()

    quote do
      unquote(decimal)
    end
  end

  describe "create_scenario/1" do
    test "should update #{@holder_name} submitted_to_carrier" do
      assert %{id: holder_id, submitted_to_carrier: false} =
               insert(:holder, submitted_to_carrier: false)

      assert {:ok, _scenario} =
               Scenarios.create_scenario(%{
                 :"#{@holder_name}_id" => holder_id,
                 name: "Name",
                 submitted_to_carrier: true
               })

      assert %{submitted_to_carrier: true} = Holders.get_holder(holder_id)
    end

    test "can create scenario with holder_id and name only" do
      %{id: holder_id} = insert(:holder)
      assert {:error, changeset} = Scenarios.create_scenario(%{})
      assert %{name: ["can't be blank"]} = errors_on(changeset)

      assert {:ok, %Scenario{name: "scenario 1"}} =
               Scenarios.create_scenario(%{:"#{@holder_name}_id" => holder_id, name: "scenario 1"})
    end

    test "should put payoff_order to be debt_snowball based on debts order" do
      %{id: holder_id} = insert(:holder)

      assert {:ok, scenario = %Scenario{payoff_order: :debt_snowball}} =
               Scenarios.create_scenario(%{
                 :"#{@holder_name}_id" => holder_id,
                 name: "scenario 1",
                 debts: [
                   %{
                     interest_rate: 15,
                     minimum_monthly_payment: 250,
                     actual_monthly_payment: 350,
                     name: "Debt Name",
                     payoff_order: 0,
                     principal: 2000
                   },
                   %{
                     interest_rate: 15,
                     minimum_monthly_payment: 250,
                     actual_monthly_payment: 350,
                     name: "Debt Name",
                     payoff_order: 2,
                     principal: 3000
                   }
                 ],
                 mortgages: [
                   %{
                     principal: 2500,
                     interest_rate: 15,
                     monthly_payment: 300,
                     months_remaining: 32,
                     payoff_order: 1,
                     name: "Name",
                     total_monthly_payment: 60,
                     type: :arm
                   }
                 ]
               })
      assert scenario |> Map.get(:"#{@holder_name}_id") == holder_id
    end

    test "should put payoff_order to be debt_avalanche based on debts order" do
      %{id: holder_id} = insert(:holder)

      assert {:ok, %Scenario{payoff_order: :debt_avalanche}} =
               Scenarios.create_scenario(%{
                 :"#{@holder_name}_id" => holder_id,
                 name: "scenario 1",
                 debts: [
                   %{
                     interest_rate: 25,
                     minimum_monthly_payment: 250,
                     actual_monthly_payment: 350,
                     name: "Debt Name",
                     payoff_order: 0,
                     principal: 5000
                   },
                   %{
                     interest_rate: 15,
                     minimum_monthly_payment: 250,
                     actual_monthly_payment: 350,
                     name: "Debt Name",
                     payoff_order: 1,
                     principal: 3000
                   }
                 ],
                 mortgages: [
                   %{
                     principal: 2500,
                     interest_rate: 5,
                     monthly_payment: 300,
                     months_remaining: 32,
                     payoff_order: 2,
                     name: "Name",
                     total_monthly_payment: 60,
                     type: :arm
                   }
                 ]
               })
    end

    test "should put payoff_order to be custom based on debts order" do
      %{id: holder_id} = insert(:holder)

      assert {:ok, %Scenario{payoff_order: :custom}} =
               Scenarios.create_scenario(%{
                 :"#{@holder_name}_id" => holder_id,
                 name: "scenario 1",
                 debts: [
                   %{
                     interest_rate: 10,
                     minimum_monthly_payment: 250,
                     actual_monthly_payment: 350,
                     name: "Debt Name",
                     payoff_order: 0,
                     principal: 5000
                   },
                   %{
                     interest_rate: 25,
                     minimum_monthly_payment: 250,
                     actual_monthly_payment: 350,
                     name: "Debt Name",
                     payoff_order: 1,
                     principal: 1000
                   },
                   %{
                     interest_rate: 15,
                     minimum_monthly_payment: 250,
                     actual_monthly_payment: 350,
                     name: "Debt Name",
                     payoff_order: 2,
                     principal: 3000
                   }
                 ]
               })
    end

    test "parse direct fields" do
      %{id: holder_id} = insert(:holder)

      assert {:ok,
              %Scenario{
                goals: "Goals",
                name: "Name",
                savings_account_interest_rate: quoted_decimal(3),
                submitted_to_carrier: true
              }} =
               Scenarios.create_scenario(%{
                 :"#{@holder_name}_id" => holder_id,
                 goals: "Goals",
                 name: "Name",
                 savings_account_interest_rate: 3,
                 submitted_to_carrier: true
               })
    end

    test "can parse debts" do
      %{id: holder_id} = insert(:holder)

      assert {:ok, %Scenario{debts: [debt]}} =
               Scenarios.create_scenario(%{
                 :"#{@holder_name}_id" => holder_id,
                 name: "scenario 1",
                 debts: [
                   %{
                     interest_rate: 15,
                     minimum_monthly_payment: 250,
                     actual_monthly_payment: 350,
                     name: "Debt Name",
                     payoff_order: 1,
                     principal: 2000
                   }
                 ]
               })

      assert %Debt{
               interest_rate: quoted_decimal(15),
               minimum_monthly_payment: quoted_decimal(250),
               actual_monthly_payment: quoted_decimal(350),
               name: "Debt Name",
               payoff_order: 1,
               principal: quoted_decimal(2000)
             } = debt
    end

    test "should sum all overrage" do
      %{id: holder_id} = insert(:holder)

      assert {:ok,
              %Scenario{
                debt_overpayment_available: quoted_decimal("100.00"),
                total_debt_overpayment: quoted_decimal(75)
              }} =
               Scenarios.create_scenario(%{
                 :"#{@holder_name}_id" => holder_id,
                 name: "scenario 1",
                 total_debt_overpayment: 75,
                 debt_overpayment_available: Decimal.new("100.00"),
                 debts: [
                   %{
                     interest_rate: 15,
                     minimum_monthly_payment: 250,
                     actual_monthly_payment: 350,
                     name: "Debt Name",
                     payoff_order: 1,
                     principal: 2000
                   },
                   %{
                     interest_rate: 15,
                     minimum_monthly_payment: 250,
                     actual_monthly_payment: 250,
                     name: "Debt Name",
                     payoff_order: 1,
                     principal: 2000
                   }
                 ]
               })
    end

    test "should debt calculate overrage" do
      assert {:error, %{changes: %{debts: [%{changes: %{overrage: quoted_decimal(0)}}]}}} =
               Scenarios.create_scenario(%{
                 name: "scenario 1",
                 debts: [
                   %{
                     minimum_monthly_payment: 100,
                     actual_monthly_payment: 100
                   }
                 ]
               })

      assert {:error, %{changes: %{debts: [%{changes: %{overrage: quoted_decimal(1)}}]}}} =
               Scenarios.create_scenario(%{
                 name: "scenario 1",
                 debts: [
                   %{
                     minimum_monthly_payment: 100,
                     actual_monthly_payment: 101
                   }
                 ]
               })

      assert {:error, %{changes: %{debts: [%{changes: %{overrage: quoted_decimal(0.0)}}]}}} =
               Scenarios.create_scenario(%{
                 name: "scenario 1",
                 debts: [
                   %{
                     minimum_monthly_payment: 101,
                     actual_monthly_payment: 100
                   }
                 ]
               })
    end

    test "can parse income sources" do
      %{id: holder_id} = insert(:holder)

      for type <- ~w(wages_or_salary social_security pension
                    investment_income rental_income other_income)a do
        assert {:ok, %Scenario{income_sources: [income_source]}} =
                 Scenarios.create_scenario(%{
	                 :"#{@holder_name}_id" => holder_id,
                   name: "scenario 1",
                   income_sources: [
                     %{name: "income source", type: type, monthly_income: 1000}
                   ]
                 })

        assert %IncomeSource{
                 name: "income source",
                 type: ^type,
                 monthly_income: quoted_decimal(1000)
               } = income_source
      end
    end

    test "can parse investments" do
      %{id: holder_id} = insert(:holder)

      for account_type <- ~w(qualified non_qualified savings)a do
        assert {:ok, %Scenario{investments: [investment]}} =
                 Scenarios.create_scenario(%{
	                 :"#{@holder_name}_id" => holder_id,
                   name: "scenario 1",
                   investments: [
                     %{
                       account_type: account_type,
                       account_value: 25_000,
                       monthly_contribution: 150,
                       name: "name"
                     }
                   ]
                 })

        assert %Investment{
                 account_type: ^account_type,
                 account_value: quoted_decimal(25_000),
                 monthly_contribution: quoted_decimal(150),
                 name: "name"
               } = investment
      end
    end

    test "should calculate total_investments_monthly_premium_redirecting" do
      %{id: holder_id} = insert(:holder)

      assert {:ok,
              %Scenario{
                total_investments_monthly_premium_redirecting: quoted_decimal(75),
                investments_monthly_premium_redirecting_available: quoted_decimal("750.00")
              }} =
               Scenarios.create_scenario(%{
                 :"#{@holder_name}_id" => holder_id,
                 name: "scenario 1",
                 total_investments_monthly_premium_redirecting: 75,
                 investments_monthly_premium_redirecting_available: quoted_decimal("750.00"),
                 investments: [
                   %{
                     account_type: "qualified",
                     account_value: 25_000,
                     monthly_contribution: 150,
                     name: "name"
                   },
                   %{
                     account_type: "qualified",
                     account_value: 25_000,
                     monthly_contribution: 600,
                     name: "name"
                   }
                 ]
               })
    end

    test "should calculate total_investments_cash_value_redirecting" do
      %{id: holder_id} = insert(:holder)

      assert {:ok,
              %Scenario{
                total_investments_cash_value_redirecting: quoted_decimal(750),
                investments_cash_value_redirecting_available: quoted_decimal("55000.00")
              }} =
               Scenarios.create_scenario(%{
                 :"#{@holder_name}_id" => holder_id,
                 name: "scenario 1",
                 total_investments_cash_value_redirecting: 750,
                 investments_cash_value_redirecting_available: quoted_decimal("55000.00"),
                 investments: [
                   %{
                     account_type: "qualified",
                     account_value: 30_000,
                     monthly_contribution: 150,
                     name: "name"
                   },
                   %{
                     account_type: "qualified",
                     account_value: 25_000,
                     monthly_contribution: 600,
                     name: "name"
                   }
                 ]
               })
    end

    test "can parse insurances" do
      %{id: holder_id} = insert(:holder)

      for type <- ~w(permanent term)a do
        for rating <- ~w(standard standard_non_tobacco tobacco)a do
          assert {:ok, %Scenario{insurances: [insurance]}} =
                   Scenarios.create_scenario(%{
		                 :"#{@holder_name}_id" => holder_id,
                     name: "scenario 1",
                     insurances: [
                       %{
                         name: "Insurance 1",
                         monthly_premium: 2000,
                         cash_value: 150,
                         death_benefit: 500_000,
                         rating: rating,
                         type: type
                       }
                     ]
                   })

          assert %Insurance{
                   name: "Insurance 1",
                   monthly_premium: quoted_decimal(2000),
                   cash_value: quoted_decimal(150),
                   death_benefit: quoted_decimal(500_000),
                   rating: ^rating,
                   type: ^type
                 } = insurance
        end
      end
    end

    test "should calculates total_insurance_monthly_premium_redirecting" do
      %{id: holder_id} = insert(:holder)

      assert {:ok,
              %Scenario{
                total_insurance_monthly_premium_redirecting: quoted_decimal(200),
                insurance_monthly_premium_redirecting_available: quoted_decimal("7000.00")
              }} =
               Scenarios.create_scenario(%{
                 :"#{@holder_name}_id" => holder_id,
                 name: "scenario 1",
                 total_insurance_monthly_premium_redirecting: 200,
                 insurance_monthly_premium_redirecting_available: quoted_decimal("7000.00"),
                 insurances: [
                   %{
                     name: "Insurance 1",
                     monthly_premium: 2000,
                     cash_value: 150,
                     death_benefit: 500_000,
                     rating: "standard",
                     type: "permanent"
                   },
                   %{
                     name: "Insurance 1",
                     monthly_premium: 5000,
                     cash_value: 1500,
                     death_benefit: 500_000,
                     rating: "standard",
                     type: "permanent"
                   }
                 ]
               })
    end

    test "should calculates total_insurance_cash_value_redirecting" do
      %{id: holder_id} = insert(:holder)

      assert {:ok,
              %Scenario{
                total_insurance_cash_value_redirecting: quoted_decimal(200),
                insurance_cash_value_redirecting_available: quoted_decimal("1650.00")
              }} =
               Scenarios.create_scenario(%{
                 :"#{@holder_name}_id" => holder_id,
                 name: "scenario 1",
                 total_insurance_cash_value_redirecting: 200,
                 insurance_cash_value_redirecting_available: quoted_decimal("1650.00"),
                 insurances: [
                   %{
                     name: "Insurance 1",
                     monthly_premium: 2000,
                     cash_value: 150,
                     death_benefit: 500_000,
                     rating: "standard",
                     type: "permanent"
                   },
                   %{
                     name: "Insurance 1",
                     monthly_premium: 5000,
                     cash_value: 1500,
                     death_benefit: 500_000,
                     rating: "standard",
                     type: "permanent"
                   }
                 ]
               })
    end

    test "can parse mortgages" do
      %{id: holder_id} = insert(:holder)

      for type <- ~w(fixed_term arm interest_only)a do
        assert {:ok, %Scenario{mortgages: [mortgage]}} =
                 Scenarios.create_scenario(%{
	                 :"#{@holder_name}_id" => holder_id,
                   name: "scenario 1",
                   mortgages: [
                     %{
                       principal: 1000,
                       interest_rate: 20,
                       monthly_payment: 300,
                       name: "Name",
                       monthly_overpayment_to_principal: 70,
                       type: type
                     }
                   ]
                 })

        assert %Mortgage{
                 interest_rate: quoted_decimal(20),
                 monthly_payment: quoted_decimal(300),
                 months_remaining: quoted_decimal(3),
                 name: "Name",
                 monthly_overpayment_to_principal: quoted_decimal(70),
                 total_monthly_payment: quoted_decimal(370),
                 type: ^type
               } = mortgage
      end
    end

    test "create scenario with invalid debt" do
      %{id: holder_id} = insert(:holder)

      assert {:ok, %Scenario{is_valid: false, debts: [%Debt{}]}} =
               Scenarios.create_scenario(%{
                 :"#{@holder_name}_id" => holder_id,
                 name: "Scenario",
                 debts: [%{}]
               })
    end

    test "create scenario with invalid mortgage" do
      %{id: holder_id} = insert(:holder)
      assert {:ok, %Scenario{is_valid: false, mortgages: [%Mortgage{}]}} =
               Scenarios.create_scenario(%{
                 :"#{@holder_name}_id" => holder_id,
                 name: "Scenario",
                 mortgages: [%{}]
               })
    end

    test "create scenario with invalid insurance" do
      %{id: holder_id} = insert(:holder)
      assert {:ok, %Scenario{is_valid: false, insurances: [%Insurance{}]}} =
               Scenarios.create_scenario(%{
                 :"#{@holder_name}_id" => holder_id,
                 name: "Scenario",
                 insurances: [%{}]
               })
    end

    test "create scenario with invalid income source" do
      %{id: holder_id} = insert(:holder)
      assert {:ok, %Scenario{is_valid: false, income_sources: [%IncomeSource{}]}} =
               Scenarios.create_scenario(%{
                 :"#{@holder_name}_id" => holder_id,
                 name: "Scenario",
                 income_sources: [%{}]
               })
    end

    test "create scenario with invalid investment" do
      %{id: holder_id} = insert(:holder)
      assert {:ok, %Scenario{is_valid: false, investments: [%Investment{}]}} =
               Scenarios.create_scenario(%{
                 :"#{@holder_name}_id" => holder_id,
                 name: "Scenario",
                 investments: [%{}]
               })
    end
  end

  describe "update_scenario/2" do
    setup do
      %{id: holder_id} = insert(:holder)

      assert {:ok, scenario} =
               Scenarios.create_scenario(%{
                 :"#{@holder_name}_id" => holder_id,
                 name: "scenario 1",
                 payoff_order: :debt_avalanche,
                 goals: "Goals",
                 savings_account_interest_rate: 3,
                 submitted_to_carrier: true,
                 debts: [
                   %{
                     interest_rate: 15,
                     actual_monthly_payment: 250,
                     minimum_monthly_payment: 250,
                     name: "Debt Name",
                     payoff_order: 1,
                     principal: 2000,
                     transition_date: ~D[2021-12-31]
                   }
                 ],
                 income_sources: [
                   %{name: "income source", type: :wages_or_salary, monthly_income: 1000}
                 ],
                 investments: [
                   %{
                     account_type: :qualified,
                     account_value: 25_000,
                     monthly_contribution: 150,
                     name: "name"
                   }
                 ],
                 insurances: [
                   %{
                     name: "Insurance 1",
                     monthly_premium: 2000,
                     cash_value: 150,
                     death_benefit: 500_000,
                     rating: :standard,
                     type: :permanent
                   }
                 ],
                 mortgages: [
                   %{
                     interest_rate: 20,
                     monthly_payment: 300,
                     months_remaining: 32,
                     name: "Name",
                     total_monthly_payment: 60_000,
                     type: :fixed_term,
                     principal: 500_000
                   }
                 ]
               })

      {:ok, scenario: scenario}
    end

    test "should update holder submitted_to_carrier" do
      assert %{id: holder_id, submitted_to_carrier: false} =
               insert(:holder, submitted_to_carrier: false)

      assert {:ok, scenario} =
               Scenarios.create_scenario(%{
                 :"#{@holder_name}_id" => holder_id,
                 name: "Name",
                 submitted_to_carrier: false
               })

      assert %{submitted_to_carrier: false} = Holders.get_holder(holder_id)

      assert {:ok, _scenario} =
               Scenarios.update_scenario(scenario, %{
                 submitted_to_carrier: true
               })

      assert %{submitted_to_carrier: true} = Holders.get_holder(holder_id)
    end

    test "should not change holder_id", %{
      scenario: %{name: "scenario 1"} = scenario
    } do
      %{id: holder_id} = insert(:holder)
      assert {:ok, %Scenario{}} =
               Scenarios.update_scenario(scenario, %{
                 :"#{@holder_name}_id" => holder_id
               })
    end

    test "update direct fields", %{scenario: scenario} do
      assert {:ok,
              %Scenario{
                goals: "new Goals",
                name: "new Name",
                savings_account_interest_rate: quoted_decimal(5),
                submitted_to_carrier: false
              }} =
               Scenarios.update_scenario(scenario, %{
                 goals: "new Goals",
                 name: "new Name",
                 savings_account_interest_rate: 5,
                 submitted_to_carrier: false
               })
    end

    test "can replace debts", %{scenario: %Scenario{debts: [%{id: id}]} = scenario} do
      assert {:ok, %Scenario{debts: [debt]}} =
               Scenarios.update_scenario(scenario, %{
                 debts: [
                   %{
                     interest_rate: 10,
                     minimum_monthly_payment: 300,
                     actual_monthly_payment: 300,
                     name: "New Debt Name",
                     payoff_order: 1,
                     principal: 1000
                   }
                 ]
               })

      assert %Debt{
               id: new_id,
               interest_rate: quoted_decimal(10),
               minimum_monthly_payment: quoted_decimal(300),
               actual_monthly_payment: quoted_decimal(300),
               name: "New Debt Name",
               payoff_order: 1,
               principal: quoted_decimal(1000)
             } = debt

      assert new_id != id
    end

    test "can update debts", %{scenario: %Scenario{debts: [%{id: id}]} = scenario} do
      assert {:ok, %Scenario{debts: [debt]}} =
               Scenarios.update_scenario(scenario, %{
                 debts: [
                   %{
                     id: id,
                     interest_rate: 10,
                     minimum_monthly_payment: 300,
                     name: "New Debt Name",
                     payoff_order: 1,
                     principal: 1000
                   }
                 ]
               })

      assert %Debt{
               id: ^id,
               interest_rate: quoted_decimal(10),
               minimum_monthly_payment: quoted_decimal(300),
               name: "New Debt Name",
               payoff_order: 1,
               principal: quoted_decimal(1000)
             } = debt
    end

    test "can update income sources", %{
      scenario: %Scenario{income_sources: [%{id: id}]} = scenario
    } do
      assert {:ok, %Scenario{income_sources: [income_source]}} =
               Scenarios.update_scenario(scenario, %{
                 income_sources: [
                   %{id: id, type: :social_security, monthly_income: 250}
                 ]
               })

      assert %IncomeSource{
               id: ^id,
               type: :social_security,
               monthly_income: quoted_decimal(250)
             } = income_source
    end

    test "can replace income sources", %{
      scenario: %Scenario{income_sources: [%{id: id}]} = scenario
    } do
      assert {:ok, %Scenario{income_sources: [income_source]}} =
               Scenarios.update_scenario(scenario, %{
                 income_sources: [
                   %{name: "income source", type: :social_security, monthly_income: 250}
                 ]
               })

      assert %IncomeSource{
               id: new_id,
               type: :social_security,
               monthly_income: quoted_decimal(250)
             } = income_source

      assert new_id != id
    end

    test "can update investments", %{scenario: %Scenario{investments: [%{id: id}]} = scenario} do
      assert {:ok, %Scenario{investments: [investment]}} =
               Scenarios.update_scenario(scenario, %{
                 investments: [
                   %{
                     id: id,
                     account_type: :qualified,
                     account_value: 1000,
                     monthly_contribution: 75,
                     name: "new name"
                   }
                 ]
               })

      assert %Investment{
               id: ^id,
               account_type: :qualified,
               account_value: quoted_decimal(1000),
               monthly_contribution: quoted_decimal(75),
               name: "new name"
             } = investment
    end

    test "can replace investments", %{scenario: %Scenario{investments: [%{id: id}]} = scenario} do
      assert {:ok, %Scenario{investments: [investment]}} =
               Scenarios.update_scenario(scenario, %{
                 investments: [
                   %{
                     account_type: :qualified,
                     account_value: 1000,
                     monthly_contribution: 75,
                     name: "new name"
                   }
                 ]
               })

      assert %Investment{
               id: new_id,
               account_type: :qualified,
               account_value: quoted_decimal(1000),
               monthly_contribution: quoted_decimal(75),
               name: "new name"
             } = investment

      assert new_id != id
    end

    test "can update insurances", %{scenario: %Scenario{insurances: [%{id: id}]} = scenario} do
      assert {:ok, %Scenario{insurances: [insurance]}} =
               Scenarios.update_scenario(scenario, %{
                 insurances: [
                   %{
                     id: id,
                     name: "Insurance 2",
                     monthly_premium: 1500,
                     cash_value: 75,
                     death_benefit: 200_000,
                     rating: :tobacco,
                     type: :term
                   }
                 ]
               })

      assert %Insurance{
               id: ^id,
               name: "Insurance 2",
               monthly_premium: quoted_decimal(1500),
               cash_value: quoted_decimal(75),
               death_benefit: quoted_decimal(200_000),
               rating: tobacco,
               type: term
             } = insurance
    end

    test "can replace insurances", %{scenario: %Scenario{insurances: [%{id: id}]} = scenario} do
      assert {:ok, %Scenario{insurances: [insurance]}} =
               Scenarios.update_scenario(scenario, %{
                 insurances: [
                   %{
                     name: "Insurance 2",
                     monthly_premium: 1500,
                     cash_value: 75,
                     death_benefit: 200_000,
                     rating: :tobacco,
                     type: :term
                   }
                 ]
               })

      assert %Insurance{
               name: "Insurance 2",
               id: new_id,
               monthly_premium: quoted_decimal(1500),
               cash_value: quoted_decimal(75),
               death_benefit: quoted_decimal(200_000),
               rating: tobacco,
               type: term
             } = insurance

      assert id != new_id
    end

    test "can update mortgages", %{scenario: %Scenario{mortgages: [%{id: id}]} = scenario} do
      assert {:ok, %Scenario{mortgages: [mortgage]}} =
               Scenarios.update_scenario(scenario, %{
                 mortgages: [
                   %{
                     id: id,
                     interest_rate: 10,
                     monthly_payment: 15_000,
                     name: "Name",
                     monthly_overpayment_to_principal: 50,
                     type: :fixed_term
                   }
                 ]
               })

      assert %Mortgage{
               id: ^id,
               interest_rate: quoted_decimal(10),
               monthly_payment: quoted_decimal(15_000),
               months_remaining: quoted_decimal(39),
               name: "Name",
               monthly_overpayment_to_principal: quoted_decimal(50),
               total_monthly_payment: quoted_decimal(15_050),
               type: :fixed_term
             } = mortgage
    end

    test "can replace mortgages", %{scenario: %Scenario{mortgages: [%{id: id}]} = scenario} do
      assert {:ok, %Scenario{mortgages: [mortgage]}} =
               Scenarios.update_scenario(scenario, %{
                 mortgages: [
                   %{
                     interest_rate: 10,
                     monthly_payment: 25_000,
                     principal: 250_000,
                     months_remaining: 12,
                     name: "Name",
                     monthly_overpayment_to_principal: 50,
                     type: :fixed_term
                   }
                 ]
               })

      assert %Mortgage{
               id: new_id,
               interest_rate: quoted_decimal(10),
               monthly_payment: quoted_decimal(25_000),
               principal: quoted_decimal(250_000),
               months_remaining: quoted_decimal(11),
               name: "Name",
               monthly_overpayment_to_principal: quoted_decimal(50),
               total_monthly_payment: quoted_decimal(25_050),
               type: :fixed_term
             } = mortgage

      assert new_id != id
    end
  end

  describe "change_scenario/2" do
    test "should set total_debt_overpayment = debt_overpayment_available when debt_overpayment_available is changed" do
      assert %Ecto.Changeset{
               changes: %{
                 total_debt_overpayment: quoted_decimal("100.00")
               }
             } =
               Scenarios.change_scenario(%Scenario{}, %{
                 total_debt_overpayment: Decimal.new("150.00"),
                 debts: [
                   %{
                     interest_rate: 15,
                     minimum_monthly_payment: 250,
                     actual_monthly_payment: 350,
                     name: "Debt Name",
                     payoff_order: 1,
                     principal: 2000
                   },
                   %{
                     interest_rate: 15,
                     minimum_monthly_payment: 250,
                     actual_monthly_payment: 250,
                     name: "Debt Name",
                     payoff_order: 1,
                     principal: 2000
                   }
                 ]
               })
    end

    test "should allow modify total_debt_overpayment when debt_overpayment_available is not changed" do
      assert %Ecto.Changeset{
               changes: %{
                 debt_overpayment_available: quoted_decimal("100.00"),
                 total_debt_overpayment: quoted_decimal("50.00")
               }
             } =
               Scenarios.change_scenario(%Scenario{}, %{
                 debt_overpayment_available: quoted_decimal("100.00"),
                 total_debt_overpayment: quoted_decimal("50.00"),
                 debts: [
                   %{
                     interest_rate: 15,
                     minimum_monthly_payment: 250,
                     actual_monthly_payment: 350,
                     name: "Debt Name",
                     payoff_order: 1,
                     principal: 2000
                   },
                   %{
                     interest_rate: 15,
                     minimum_monthly_payment: 250,
                     actual_monthly_payment: 250,
                     name: "Debt Name",
                     payoff_order: 1,
                     principal: 2000
                   }
                 ]
               })
    end

    test "should set total_insurance_monthly_premium_redirecting = insurance_monthly_premium_redirecting_available when insurance_monthly_premium_redirecting_available is changed" do
      assert %Ecto.Changeset{
               changes: %{
                 total_insurance_monthly_premium_redirecting: quoted_decimal("3000.00"),
                 insurance_monthly_premium_redirecting_available: quoted_decimal("3000.00")
               }
             } =
               Scenarios.change_scenario(%Scenario{}, %{
                 total_insurance_monthly_premium_redirecting: quoted_decimal("150.00"),
                 insurance_monthly_premium_redirecting_available: quoted_decimal("150.00"),
                 insurances: [
                   %{
                     name: "Insurance 1",
                     monthly_premium: 2000,
                     cash_value: 0,
                     death_benefit: 500_000,
                     rating: "standard",
                     type: "permanent"
                   },
                   %{
                     name: "Insurance 1",
                     monthly_premium: 1000,
                     cash_value: 500,
                     death_benefit: 500_000,
                     rating: "standard",
                     type: "permanent"
                   }
                 ]
               })
    end

    test "should allow modify total_insurance_monthly_premium_redirecting when insurance_monthly_premium_redirecting_available is not changed" do
      assert %Ecto.Changeset{
               changes: %{}
             } =
               Scenarios.change_scenario(%Scenario{}, %{
                 insurances: [
                   %{
                     name: "Insurance 1",
                     monthly_premium: 2000,
                     cash_value: 150,
                     death_benefit: 500_000,
                     rating: "standard",
                     type: "permanent"
                   },
                   %{
                     name: "Insurance 1",
                     monthly_premium: 5000,
                     cash_value: 1500,
                     death_benefit: 500_000,
                     rating: "standard",
                     type: "permanent"
                   }
                 ]
               })
    end

    test "should set total_insurance_cash_value_redirecting = insurance_cash_value_redirecting_available when insurance_cash_value_redirecting_available is changed" do
      assert %Ecto.Changeset{
               changes: %{
                 insurance_cash_value_redirecting_available: quoted_decimal("500.00"),
                 total_insurance_cash_value_redirecting: quoted_decimal("500.00")
               }
             } =
               Scenarios.change_scenario(%Scenario{}, %{
                 insurance_cash_value_redirecting_available: quoted_decimal("150.00"),
                 total_insurance_cash_value_redirecting: quoted_decimal("150.00"),
                 insurances: [
                   %{
                     name: "Insurance 1",
                     monthly_premium: 2000,
                     cash_value: 0,
                     death_benefit: 500_000,
                     rating: "standard",
                     type: "permanent"
                   },
                   %{
                     name: "Insurance 1",
                     monthly_premium: 5000,
                     cash_value: 500,
                     death_benefit: 500_000,
                     rating: "standard",
                     type: "permanent"
                   }
                 ]
               })
    end

    test "should allow modify total_insurance_cash_value_redirecting when insurance_cash_value_redirecting_available is not changed" do
      assert %Ecto.Changeset{
               changes: %{
                 insurance_cash_value_redirecting_available: quoted_decimal("500.00"),
                 total_insurance_cash_value_redirecting: quoted_decimal("250.00")
               }
             } =
               Scenarios.change_scenario(%Scenario{}, %{
                 insurance_cash_value_redirecting_available: Decimal.new("500.00"),
                 total_insurance_cash_value_redirecting: Decimal.new("250.00"),
                 insurances: [
                   %{
                     name: "Insurance 1",
                     monthly_premium: 2000,
                     cash_value: 0,
                     death_benefit: 500_000,
                     rating: "standard",
                     type: "permanent"
                   },
                   %{
                     name: "Insurance 1",
                     monthly_premium: 1000,
                     cash_value: 500,
                     death_benefit: 500_000,
                     rating: "standard",
                     type: "permanent"
                   }
                 ]
               })
    end

    test "should set total_investments_monthly_premium_redirecting = investments_monthly_premium_redirecting_available when investments_monthly_premium_redirecting_available is changed" do
      assert %Ecto.Changeset{
               changes: %{
                 investments_monthly_premium_redirecting_available: quoted_decimal("500.00"),
                 total_investments_monthly_premium_redirecting: quoted_decimal("500.00")
               }
             } =
               Scenarios.change_scenario(%Scenario{}, %{
                 investments_monthly_premium_redirecting_available: quoted_decimal("150.00"),
                 total_investments_monthly_premium_redirecting: quoted_decimal("150.00"),
                 investments: [
                   %{
                     account_type: "qualified",
                     account_value: 10_000,
                     monthly_contribution: 100,
                     name: "name"
                   },
                   %{
                     account_type: "qualified",
                     account_value: 10_000,
                     monthly_contribution: 400,
                     name: "name"
                   }
                 ]
               })
    end

    test "should allow modify total_investments_monthly_premium_redirecting when investments_monthly_premium_redirecting_available is not changed" do
      assert %Ecto.Changeset{
               changes: %{
                 investments_monthly_premium_redirecting_available: quoted_decimal("500.00"),
                 total_investments_monthly_premium_redirecting: quoted_decimal("250.00")
               }
             } =
               Scenarios.change_scenario(%Scenario{}, %{
                 investments_monthly_premium_redirecting_available: Decimal.new("500.00"),
                 total_investments_monthly_premium_redirecting: Decimal.new("250.00"),
                 investments: [
                   %{
                     account_type: "qualified",
                     account_value: 10_000,
                     monthly_contribution: 100,
                     name: "name"
                   },
                   %{
                     account_type: "qualified",
                     account_value: 10_000,
                     monthly_contribution: 400,
                     name: "name"
                   }
                 ]
               })
    end

    test "should set total_investments_cash_value_redirecting = investments_cash_value_redirecting_available when investments_cash_value_redirecting_available is changed" do
      assert %Ecto.Changeset{
               changes: %{
                 investments_cash_value_redirecting_available: quoted_decimal("20000.00"),
                 total_investments_cash_value_redirecting: quoted_decimal("20000.00")
               }
             } =
               Scenarios.change_scenario(%Scenario{}, %{
                 investments_cash_value_redirecting_available: quoted_decimal("150.00"),
                 total_investments_cash_value_redirecting: quoted_decimal("150.00"),
                 investments: [
                   %{
                     account_type: "qualified",
                     account_value: 10_000,
                     monthly_contribution: 100,
                     name: "name"
                   },
                   %{
                     account_type: "qualified",
                     account_value: 10_000,
                     monthly_contribution: 400,
                     name: "name"
                   }
                 ]
               })
    end

    test "should allow modify total_investments_cash_value_redirecting when investments_cash_value_redirecting_available is not changed" do
      assert %Ecto.Changeset{
               changes: %{
                 investments_cash_value_redirecting_available: quoted_decimal("20000.00"),
                 total_investments_cash_value_redirecting: quoted_decimal("5000.00")
               }
             } =
               Scenarios.change_scenario(%Scenario{}, %{
                 investments_cash_value_redirecting_available: Decimal.new("20000.00"),
                 total_investments_cash_value_redirecting: Decimal.new("5000.00"),
                 investments: [
                   %{
                     account_type: "qualified",
                     account_value: 10_000,
                     monthly_contribution: 100,
                     name: "name"
                   },
                   %{
                     account_type: "qualified",
                     account_value: 10_000,
                     monthly_contribution: 400,
                     name: "name"
                   }
                 ]
               })
    end

    test "should set total_mortgage_overpayment_redirecting = mortgage_overpayment_redirecting_available when mortgage_overpayment_redirecting_available is changed" do
      assert %Ecto.Changeset{
               changes: %{
                 mortgage_overpayment_redirecting_available: quoted_decimal("120.00"),
                 total_mortgage_overpayment_redirecting: quoted_decimal("120.00")
               }
             } =
               Scenarios.change_scenario(%Scenario{}, %{
                 mortgage_overpayment_redirecting_available: quoted_decimal("100.00"),
                 total_mortgage_overpayment_redirecting: quoted_decimal("100.00"),
                 mortgages: [
                   %{
                     principal: 2500,
                     interest_rate: 5,
                     monthly_payment: 300,
                     months_remaining: 32,
                     payoff_order: 2,
                     name: "Name",
                     monthly_overpayment_to_principal: 60,
                     type: :arm
                   },
                   %{
                     principal: 2500,
                     interest_rate: 5,
                     monthly_payment: 300,
                     months_remaining: 32,
                     payoff_order: 2,
                     name: "Name",
                     monthly_overpayment_to_principal: 60,
                     type: :arm
                   }
                 ]
               })
    end

    test "should allow modify total_mortgage_overpayment_redirecting when mortgage_overpayment_redirecting_available is not changed" do
      assert %Ecto.Changeset{
               changes: %{
                 mortgage_overpayment_redirecting_available: quoted_decimal("120.00"),
                 total_mortgage_overpayment_redirecting: quoted_decimal("100.00")
               }
             } =
               Scenarios.change_scenario(%Scenario{}, %{
                 mortgage_overpayment_redirecting_available: quoted_decimal("120.00"),
                 total_mortgage_overpayment_redirecting: quoted_decimal("100.00"),
                 mortgages: [
                   %{
                     principal: 2500,
                     interest_rate: 5,
                     monthly_payment: 300,
                     months_remaining: 32,
                     payoff_order: 2,
                     name: "Name",
                     monthly_overpayment_to_principal: 60,
                     type: :arm
                   },
                   %{
                     principal: 2500,
                     interest_rate: 5,
                     monthly_payment: 300,
                     months_remaining: 32,
                     payoff_order: 2,
                     name: "Name",
                     monthly_overpayment_to_principal: 60,
                     type: :arm
                   }
                 ]
               })
    end

    test "should allow modify total_income_redirecting" do
      assert %Ecto.Changeset{
               changes: %{
                 total_income_redirecting: quoted_decimal("70.00"),
                 income_redirecting_available: quoted_decimal("100.00")
               }
             } =
               Scenarios.change_scenario(%Scenario{}, %{
                 total_income_redirecting: quoted_decimal("70.00"),
                 income_sources: [
                   %{name: "income source 1", type: :wages_or_salary, monthly_income: 50},
                   %{name: "income source 2", type: :wages_or_salary, monthly_income: 50}
                 ]
               })
    end

    test "should total_debt_overpayment <=debt_overpayment_available and >=0" do
      %{id: holder_id} = insert(:holder)

      debts = [
        %{
          interest_rate: 15,
          minimum_monthly_payment: 250,
          actual_monthly_payment: 350,
          name: "Debt Name",
          payoff_order: 1,
          principal: 2000
        },
        %{
          interest_rate: 15,
          minimum_monthly_payment: 250,
          actual_monthly_payment: 250,
          name: "Debt Name",
          payoff_order: 1,
          principal: 2000
        }
      ]

      assert changeset =
               Scenarios.change_scenario(%{
                 :"#{@holder_name}_id" => holder_id,
                 name: "scenario 1",
                 total_debt_overpayment: 125,
                 debt_overpayment_available: 100,
                 debts: debts
               })

      assert "must be less than or equal to 100" in errors_on(changeset).total_debt_overpayment

      assert changeset =
               Scenarios.change_scenario(%{
                 :"#{@holder_name}_id" => holder_id,
                 name: "scenario 1",
                 debt_overpayment_available: 100,
                 total_debt_overpayment: -1,
                 debts: debts
               })

      assert "must be greater than or equal to 0" in errors_on(changeset).total_debt_overpayment
    end

    test "debts principal must be greater than 0" do
      assert changeset =
               Scenarios.change_scenario(%{
                 name: "scenario 1",
                 debts: [
                   %{
                     principal: 0
                   }
                 ]
               })

      assert "must be greater than 0" in get_error_for(changeset, :debts, :principal)

      assert changeset =
               Scenarios.change_scenario(%{
                 name: "scenario 1",
                 debts: [
                   %{
                     principal: 1
                   }
                 ]
               })

      refute get_error_for(changeset, :debts, :principal)
    end

    test "debts interest_rate must be greater than or equal 0" do
      assert changeset =
               Scenarios.change_scenario(%{
                 name: "scenario 1",
                 debts: [
                   %{
                     interest_rate: -1
                   }
                 ]
               })

      assert "must be greater than or equal to 0" in get_error_for(
               changeset,
               :debts,
               :interest_rate
             )

      assert changeset =
               Scenarios.change_scenario(%{
                 name: "scenario 1",
                 debts: [
                   %{
                     interest_rate: 0
                   }
                 ]
               })

      refute get_error_for(changeset, :debts, :interest_rate)
    end

    test "debts interest_rate must be less than or equal 100" do
      assert changeset =
               Scenarios.change_scenario(%{
                 name: "scenario 1",
                 debts: [
                   %{
                     interest_rate: 101
                   }
                 ]
               })

      assert "must be less than or equal to 100" in get_error_for(
               changeset,
               :debts,
               :interest_rate
             )

      assert changeset =
               Scenarios.change_scenario(%{
                 name: "scenario 1",
                 debts: [
                   %{
                     interest_rate: 100
                   }
                 ]
               })

      refute get_error_for(changeset, :debts, :interest_rate)
    end

    test "debts introductory_interest_rate must be less than or equal 100" do
      assert changeset =
               Scenarios.change_scenario(%{
                 name: "scenario 1",
                 debts: [
                   %{
                     introductory_interest_rate: 101,
                     with_introductory: true
                   }
                 ]
               })

      assert "must be less than or equal to 100" in get_error_for(
               changeset,
               :debts,
               :introductory_interest_rate
             )

      assert changeset =
               Scenarios.change_scenario(%{
                 name: "scenario 1",
                 debts: [
                   %{
                     introductory_interest_rate: 100,
                     with_introductory: true
                   }
                 ]
               })

      refute get_error_for(changeset, :debts, :introductory_interest_rate)
    end

    test "debts removes introductory_interest_rate and transition_date when with_introductory is false" do
      assert %{changes: %{debts: [changeset]}} =
               Scenarios.change_scenario(%{
                 name: "scenario 1",
                 debts: [
                   %{
                     introductory_interest_rate: 2,
                     transition_date: ~D[2021-02-10],
                     with_introductory: false
                   }
                 ]
               })

      assert changeset.changes.introductory_interest_rate == nil
      assert changeset.changes.transition_date == nil

      assert %{changes: %{debts: [changeset]}} =
               Scenarios.change_scenario(%{
                 name: "scenario 1",
                 debts: [
                   %{
                     introductory_interest_rate: 2,
                     transition_date: ~D[2021-02-10],
                     with_introductory: true
                   }
                 ]
               })

      assert Decimal.eq?(changeset.changes.introductory_interest_rate, 2)
      assert changeset.changes.transition_date == ~D[2021-02-10]
    end

    test "debts minimum_monthly_payment must be greater than 0" do
      assert changeset =
               Scenarios.change_scenario(%{
                 name: "scenario 1",
                 debts: [
                   %{
                     minimum_monthly_payment: 0
                   }
                 ]
               })

      assert "must be greater than 0" in get_error_for(
               changeset,
               :debts,
               :minimum_monthly_payment
             )

      assert changeset =
               Scenarios.change_scenario(%{
                 name: "scenario 1",
                 debts: [
                   %{
                     minimum_monthly_payment: 1
                   }
                 ]
               })

      refute get_error_for(changeset, :debts, :minimum_monthly_payment)
    end

    test "debts actual_monthly_payment >= minimum_monthly_payment" do
      assert changeset =
               Scenarios.change_scenario(%{
                 name: "scenario 1",
                 debts: [
                   %{
                     minimum_monthly_payment: 100,
                     actual_monthly_payment: 99
                   }
                 ]
               })

      assert "must be greater than or equal to 100" in get_error_for(
               changeset,
               :debts,
               :actual_monthly_payment
             )

      assert changeset =
               Scenarios.change_scenario(%{
                 name: "scenario 1",
                 debts: [
                   %{
                     minimum_monthly_payment: 100,
                     actual_monthly_payment: 100
                   }
                 ]
               })

      refute get_error_for(changeset, :debts, :actual_monthly_payment)
    end

    test "debts name is required" do
      assert changeset =
               Scenarios.change_scenario(%{
                 debts: [
                   %{
                     name: nil
                   }
                 ]
               })

      assert "can't be blank" in get_error_for(changeset, :debts, :name)

      assert changeset =
               Scenarios.change_scenario(%{
                 debts: [
                   %{
                     name: "name"
                   }
                 ]
               })

      refute get_error_for(changeset, :debts, :name)
    end

    test "should calculate months_remaining for debts" do
      assert %{changes: %{debts: [changeset]}} =
               Scenarios.change_scenario(%{
                 debts: [
                   %{
                     name: "Debt",
                     principal: 100_000,
                     interest_rate: 2,
                     minimum_monthly_payment: 5000
                   }
                 ]
               })

      assert changeset
             |> Ecto.Changeset.get_field(:months_remaining)
             |> Decimal.eq?(21)
    end

    test "income sources monthly payment >= 0" do
      assert changeset =
               Scenarios.change_scenario(%{
                 income_sources: [
                   %{monthly_income: -1}
                 ]
               })

      assert "must be greater than or equal to 0" in get_error_for(
               changeset,
               :income_sources,
               :monthly_income
             )

      assert changeset =
               Scenarios.change_scenario(%{
                 income_sources: [
                   %{monthly_income: 0}
                 ]
               })

      refute get_error_for(
               changeset,
               :income_sources,
               :monthly_income
             )
    end

    test "insurances monthly_premium must be greater than 0" do
      assert changeset =
               Scenarios.change_scenario(%{
                 insurances: [
                   %{
                     monthly_premium: 0
                   }
                 ]
               })

      assert "must be greater than 0" in get_error_for(changeset, :insurances, :monthly_premium)

      assert changeset =
               Scenarios.change_scenario(%{
                 insurances: [
                   %{
                     monthly_premium: 1
                   }
                 ]
               })

      refute get_error_for(changeset, :insurances, :monthly_premium)
    end

    test "insurances cash_value must be greater than 0" do
      assert changeset =
               Scenarios.change_scenario(%{
                 insurances: [
                   %{
                     cash_value: -1
                   }
                 ]
               })

      assert "must be greater than or equal to 0" in get_error_for(
               changeset,
               :insurances,
               :cash_value
             )

      assert changeset =
               Scenarios.change_scenario(%{
                 insurances: [
                   %{
                     cash_value: 1
                   }
                 ]
               })

      refute get_error_for(changeset, :insurances, :cash_value)
    end

    test "insurances death_benefit must be greater than or equal to 0" do
      assert changeset =
               Scenarios.change_scenario(%{
                 insurances: [
                   %{
                     death_benefit: -1
                   }
                 ]
               })

      assert "must be greater than or equal to 0" in get_error_for(
               changeset,
               :insurances,
               :death_benefit
             )

      assert changeset =
               Scenarios.change_scenario(%{
                 insurances: [
                   %{
                     death_benefit: 0
                   }
                 ]
               })

      refute get_error_for(changeset, :insurances, :death_benefit)
    end

    test "insurances monthly_premium must be > 0" do
      assert changeset =
               Scenarios.change_scenario(%{
                 insurances: [
                   %{
                     monthly_premium: -1
                   }
                 ]
               })

      assert "must be greater than 0" in get_error_for(
               changeset,
               :insurances,
               :monthly_premium
             )

      assert changeset =
               Scenarios.change_scenario(%{
                 insurances: [
                   %{
                     monthly_premium: 100
                   }
                 ]
               })

      refute get_error_for(changeset, :insurances, :monthly_premium)
    end

    test "mortgages principal > 0" do
      assert changeset =
               Scenarios.change_scenario(%{
                 mortgages: [
                   %{
                     principal: 0
                   }
                 ]
               })

      assert "must be greater than 0" in get_error_for(
               changeset,
               :mortgages,
               :principal
             )

      assert changeset =
               Scenarios.change_scenario(%{
                 mortgages: [
                   %{
                     principal: 1
                   }
                 ]
               })

      refute get_error_for(changeset, :mortgages, :principal)
    end

    test "mortgages monthly_payment >= 0" do
      assert changeset =
               Scenarios.change_scenario(%{
                 mortgages: [
                   %{
                     monthly_payment: -1
                   }
                 ]
               })

      assert "must be greater than or equal to 0" in get_error_for(
               changeset,
               :mortgages,
               :monthly_payment
             )

      assert changeset =
               Scenarios.change_scenario(%{
                 mortgages: [
                   %{
                     monthly_payment: 0
                   }
                 ]
               })

      refute get_error_for(changeset, :mortgages, :monthly_payment)
    end

    test "mortgages monthly_overpayment_to_principal>= 0" do
      assert changeset =
               Scenarios.change_scenario(%{
                 mortgages: [
                   %{
                     monthly_overpayment_to_principal: -1
                   }
                 ]
               })

      assert "must be greater than or equal to 0" in get_error_for(
               changeset,
               :mortgages,
               :monthly_overpayment_to_principal
             )

      assert changeset =
               Scenarios.change_scenario(%{
                 mortgages: [
                   %{
                     monthly_overpayment_to_principal: 0
                   }
                 ]
               })

      refute get_error_for(changeset, :mortgages, :monthly_overpayment_to_principal)
    end

    test "mortgages interest_rate >= 0" do
      assert changeset =
               Scenarios.change_scenario(%{
                 mortgages: [
                   %{
                     interest_rate: -1
                   }
                 ]
               })

      assert "must be greater than or equal to 0" in get_error_for(
               changeset,
               :mortgages,
               :interest_rate
             )

      assert changeset =
               Scenarios.change_scenario(%{
                 mortgages: [
                   %{
                     interest_rate: 1
                   }
                 ]
               })

      refute get_error_for(changeset, :mortgages, :interest_rate)
    end

    test "mortgages interest_rate <= 100" do
      assert changeset =
               Scenarios.change_scenario(%{
                 mortgages: [
                   %{
                     interest_rate: 101
                   }
                 ]
               })

      assert "must be less than or equal to 100" in get_error_for(
               changeset,
               :mortgages,
               :interest_rate
             )

      assert changeset =
               Scenarios.change_scenario(%{
                 mortgages: [
                   %{
                     interest_rate: 100
                   }
                 ]
               })

      refute get_error_for(changeset, :mortgages, :interest_rate)
    end

    test "should total_investments_monthly_premium_redirecting <= investments_monthly_premium_redirecting_available
    and >= 0" do
      investments = [
        %{
          account_type: "qualified",
          account_value: 25_000,
          monthly_contribution: 150,
          name: "name"
        },
        %{
          account_type: "qualified",
          account_value: 25_000,
          monthly_contribution: 350,
          name: "name"
        }
      ]

      assert changeset =
               Scenarios.change_scenario(%{
                 name: "scenario 1",
                 investments_monthly_premium_redirecting_available: 500,
                 total_investments_monthly_premium_redirecting: 750,
                 investments: investments
               })

      assert "must be less than or equal to 500" in errors_on(changeset).total_investments_monthly_premium_redirecting

      assert changeset =
               Scenarios.change_scenario(%{
                 name: "scenario 1",
                 investments_monthly_premium_redirecting_available: 500,
                 total_investments_monthly_premium_redirecting: -1,
                 investments: investments
               })

      assert "must be greater than or equal to 0" in errors_on(changeset).total_investments_monthly_premium_redirecting
    end

    test "insurances name is required" do
      assert changeset =
               Scenarios.change_scenario(%{
                 insurances: [
                   %{
                     name: nil
                   }
                 ]
               })

      assert "can't be blank" in get_error_for(changeset, :insurances, :name)

      assert changeset =
               Scenarios.change_scenario(%{
                 insurances: [
                   %{
                     name: "name"
                   }
                 ]
               })

      refute get_error_for(changeset, :insurances, :name)
    end

    test "should total_insurance_cash_value_redirecting <= insurance_cash_value_redirecting_available
    and insurance_cash_value_redirecting_available > 0" do
      insurances = [
        %{
          name: "Insurance 1",
          monthly_premium: 2000,
          cash_value: 0,
          death_benefit: 500_000,
          rating: "standard",
          type: "permanent"
        },
        %{
          name: "Insurance 1",
          monthly_premium: 1000,
          cash_value: 500,
          death_benefit: 500_000,
          rating: "standard",
          type: "permanent"
        }
      ]

      assert changeset =
               Scenarios.change_scenario(%{
                 name: "scenario 1",
                 insurance_cash_value_redirecting_available: 500,
                 total_insurance_cash_value_redirecting: 600,
                 insurances: insurances
               })

      assert "must be less than or equal to 500" in errors_on(changeset).total_insurance_cash_value_redirecting

      assert changeset =
               Scenarios.change_scenario(%{
                 name: "scenario 1",
                 insurance_cash_value_redirecting_available: 500,
                 total_insurance_cash_value_redirecting: -1,
                 insurances: insurances
               })

      assert "must be greater than or equal to 0" in errors_on(changeset).total_insurance_cash_value_redirecting
    end

    test "should total_investments_cash_value_redirecting <= investments_cash_value_redirecting_available
    and >= 0" do
      investments = [
        %{
          account_type: "qualified",
          account_value: 10_000,
          monthly_contribution: 100,
          name: "name"
        },
        %{
          account_type: "qualified",
          account_value: 5_000,
          monthly_contribution: 400,
          name: "name"
        }
      ]

      assert changeset =
               Scenarios.change_scenario(%{
                 name: "scenario 1",
                 investments_cash_value_redirecting_available: 15_000,
                 total_investments_cash_value_redirecting: 77_775,
                 investments: investments
               })

      assert "must be less than or equal to 15000" in errors_on(changeset).total_investments_cash_value_redirecting

      assert changeset =
               Scenarios.change_scenario(%{
                 name: "scenario 1",
                 investments_cash_value_redirecting_available: 15_000,
                 total_investments_cash_value_redirecting: -1,
                 investments: investments
               })

      assert "must be greater than or equal to 0" in errors_on(changeset).total_investments_cash_value_redirecting
    end

    test "should total_insurance_monthly_premium_redirecting <= insurance_monthly_premium_redirecting_available
    and > 0" do
      insurances = [
        %{
          name: "Insurance 1",
          monthly_premium: 2000,
          cash_value: 0,
          death_benefit: 500_000,
          rating: "standard",
          type: "permanent"
        },
        %{
          name: "Insurance 1",
          monthly_premium: 1000,
          cash_value: 500,
          death_benefit: 500_000,
          rating: "standard",
          type: "permanent"
        }
      ]

      assert changeset =
               Scenarios.change_scenario(%{
                 name: "scenario 1",
                 total_insurance_monthly_premium_redirecting: 20_000,
                 insurance_monthly_premium_redirecting_available: 3000,
                 insurances: insurances
               })

      assert "must be less than or equal to 3000" in errors_on(changeset).total_insurance_monthly_premium_redirecting

      assert changeset =
               Scenarios.change_scenario(%{
                 name: "scenario 1",
                 insurance_monthly_premium_redirecting_available: 3000,
                 total_insurance_monthly_premium_redirecting: -1,
                 insurances: insurances
               })

      assert "must be greater than or equal to 0" in errors_on(changeset).total_insurance_monthly_premium_redirecting
    end

    test "should total_income_redirecting <= income_redirecting_available
    and >= 0" do
      income_sources = [
        %{name: "income source 1", type: :wages_or_salary, monthly_income: 50},
        %{name: "income source 2", type: :wages_or_salary, monthly_income: 80}
      ]

      assert changeset =
               Scenarios.change_scenario(%{
                 name: "scenario 1",
                 total_income_redirecting: 77_775,
                 income_sources: income_sources
               })

      assert "must be less than or equal to 130.00" in errors_on(changeset).total_income_redirecting

      assert changeset =
               Scenarios.change_scenario(%{
                 name: "scenario 1",
                 total_income_redirecting: -1,
                 income_sources: income_sources
               })

      assert "must be greater than or equal to 0" in errors_on(changeset).total_income_redirecting
    end
  end

  describe "get_scenario/2" do
    test "should return scenario" do
      %{id: id} = insert(:scenario)
      assert %Scenario{id: ^id} = Scenarios.get_scenario(id)
    end

    test "should return nil for soft deleted" do
      %{id: id} = insert(:scenario, deleted_at: DateTime.utc_now())
      refute Scenarios.get_scenario(id)
    end

    test "should return soft deleted scenarios when specified" do
      %{id: id} = insert(:scenario, deleted_at: DateTime.utc_now())
      assert %Scenario{id: ^id} = Scenarios.get_scenario(id, with_deleted: true)
    end
  end

  describe "delete_scenario/1" do
    test "should soft delete scenario" do
      scenario = insert(:scenario)
      refute scenario.deleted_at

      assert {:ok, %Scenario{deleted_at: deleted_at, id: id}} =
               Scenarios.delete_scenario(scenario)

      assert deleted_at
      refute Scenarios.get_scenario(scenario.id)

      assert %Scenario{id: ^id} = Scenarios.get_scenario(scenario.id, with_deleted: true)
    end
  end

  describe "next_scenario_name/1" do
    test "should return client name + scenarios count + 1" do
      holder = insert(:holder)

      assert "#{holder.first_name} #{holder.last_name} scenario 1" ==
               Scenarios.next_scenario_name(holder)

      insert(:scenario, "#{@holder_name}": holder)

      assert "#{holder.first_name} #{holder.last_name} scenario 2" ==
               Scenarios.next_scenario_name(holder)

      insert(:scenario, "#{@holder_name}": holder)
      insert(:scenario, "#{@holder_name}": holder)
      insert(:scenario, "#{@holder_name}": holder)
      insert(:scenario, "#{@holder_name}": holder)

      insert(:scenario)

      assert "#{holder.first_name} #{holder.last_name} scenario 6" ==
               Scenarios.next_scenario_name(holder)
    end

    test "should count soft deleted scenarios" do
      holder = insert(:holder)
      insert(:scenario, "#{@holder_name}": holder, deleted_at: DateTime.utc_now())

      assert "#{holder.first_name} #{holder.last_name} scenario 2" ==
               Scenarios.next_scenario_name(holder)
    end
  end

  describe "next_scenario/1" do
    test "should create a scenario for a holder with auto filled fields" do
      holder = insert(:holder)
      name = "#{holder.first_name} #{holder.last_name} scenario 1"
      date = Date.utc_today()

      assert {:ok, %Scenario{name: ^name, dfl_start_date: ^date}} =
               Scenarios.next_scenario(holder)
    end
  end

  defp get_error_for(changeset, list, field) do
    changeset
    |> errors_on()
    |> Map.get(list)
    |> case do
      [errors | _] -> errors
      _ -> %{}
    end
    |> Map.get(field)
  end
end
