defmodule Scenarios.UtilsTest do
  use Scenarios.DataCase, asycn: true
  alias Scenarios.{Debt, Mortgage, Scenarios, Utils}

  @examples [
    {"39412.95", "10", "859.68", "59"},
    {"44312.77", "6", "1431.73", "33"},
    {"54228.73", "14", "2431.73", "25"},
    {"174027.28", "17", "5111.96", "47"},
    {"119655.27", "12", "9114.36", "15"},
    {"389798.91", "4", "9577.98", "43"},
    {"26565.3", "17", "6819.76", "5"},
    {"206857.66", "8", "8049.89", "29"},
    {"96947.42", "1", "2046.55", "49"},
    {"71529.26", "9", "9369.4", "7"},
    {"66215.58", "13", "1739.82", "49"},
    {"385347.26", "18", "9031.19", "69"},
    {"5841.19", "8", "4954.92", "1"},
    {"60222.22", "4", "6983.4", "9"},
    {"205555.89", "14", "6452.3", "41"},
    {"17455.16", "2", "5192.24", "3"},
    {"102527.41", "9", "6862.74", "15"},
    {"223002.59", "7", "6348.74", "39"},
    {"232073.1", "11", "6001.07", "47"},
    {"17885.35", "16", "7740.59", "3"},
    {"83104.14", "8", "3487.62", "27"},
    {"280352.15", "12", "7137.45", "51"},
    {"1539.77", "13", "5851.23", "1"},
    {"438029.34", "12", "8571.61", "71"},
    {"385038.48", "10", "8701.67", "55"},
    {"157529.35", "7", "5548.3", "31"},
    {"143101.37", "12", "3028.03", "65"},
    {"221751.66", "10", "7577.89", "33"},
    {"147110.22", "4", "2360.9", "69"},
    {"157411.41", "10", "8715.18", "19"},
    {"236256.01", "4", "9085.35", "27"},
    {"138873.91", "3", "3078.13", "47"},
    {"230865.14", "12", "6300.04", "45"},
    {"98238.18", "18", "7195.45", "15"},
    {"156467.66", "9", "5609.28", "31"},
    {"190655.62", "5", "9270.78", "21"},
    {"173311.94", "15", "5763.23", "37"},
    {"282650.81", "4", "6142.57", "51"},
    {"229482.96", "7", "6327.67", "41"},
    {"211795.01", "15", "5740.8", "49"},
    {"388413.16", "2", "6586.05", "63"},
    {"410857.38", "6", "9120.57", "51"},
    {"196170.02", "5", "6209.88", "33"},
    {"194229.51", "8", "3688.15", "65"},
    {"306908.64", "11", "9357.48", "39"},
    {"102166.31", "7", "7386.42", "15"},
    {"406370.24", "7", "8574.33", "55"},
    {"37918.27", "19", "1498.32", "33"},
    {"61959.55", "11", "2732.43", "25"},
    {"438103.72", "14", "8875.52", "73"},
    {"393039.08", "3", "7168.92", "59"},
    {"398743.46", "10", "9845.41", "49"},
    {"282430.82", "6", "5727.68", "57"},
    {"80652.8", "4", "4779.99", "17"},
    {"8023.39", "9", "169.29", "59"},
    {"368044.42", "1", "4627.8", "83"},
    {"439131.88", "2", "9983.96", "45"},
    {"209771.3", "1", "5811.35", "37"},
    {"112289.12", "19", "2766.93", "65"},
    {"79129.37", "8", "5707.42", "15"},
    {"114987", "3", "2440.47", "51"},
    {"3155.41", "19", "1817.23", "1"},
    {"309241.16", "15", "6431.59", "73"},
    {"341208.86", "17", "8505.34", "59"},
    {"383559.75", "6", "8253.68", "53"},
    {"170322.14", "7", "8850.98", "21"},
    {"159513.25", "2", "3335.75", "49"},
    {"148620.19", "12", "9107.78", "17"},
    {"318871.57", "10", "5455.37", "81"},
    {"240326.25", "14", "6736.66", "47"},
    {"603801.28", "9", "4846.91", "365"},
    {"532215.53", "1", "857.75", "873"},
    {"394484.19", "1", "630.08", "885"},
    {"253123.64", "3", "6173.06", "43"},
    {"27923.59", "5", "5099.42", "5"},
    {"220538.54", "18", "4485.99", "89"},
    {"438868.42", "1", "3340.11", "139"},
    {"153881.68", "2", "2921.06", "55"},
    {"136986.97", "2", "3269.4", "43"},
    {"111622.1", "17", "6521.49", "19"},
    {"186855.01", "13", "2656.69", "133"},
    {"74801.75", "10", "875.69", "149"},
    {"43807.3", "3", "2428.26", "19"},
    {"294915.02", "8", "4387.38", "89"},
    {"200651.97", "11", "5761.59", "43"},
    {"392520.06", "9", "4138.67", "167"},
    {"164360.05", "12", "3387.97", "67"},
    {"15057.11", "7", "6682.94", "3"},
    {"17847.17", "16", "4977.94", "3"},
    {"216166.66", "15", "4801.82", "67"},
    {"208543.93", "16", "5952.54", "47"},
    {"186359.29", "3", "2790.29", "73"},
    {"307886.6", "2", "3532.54", "95"},
    {"543077.7", "1", "1764.01", "355"},
    {"62945.39", "16", "5047.63", "13"},
    {"89873.33", "4", "6932.53", "13"},
    {"73840.65", "3", "2202.3", "35"},
    {"135942.83", "12", "2834.26", "65"},
    {"370731.13", "11", "9060.97", "51"},
    {"136925.29", "19", "5519.55", "31"},
    {"270087.09", "12", "5451.78", "69"},
    {"98752.08", "3", "7958.47", "13"},
    {"223008.09", "18", "9941.01", "27"},
    {"477153.82", "13", "9817.94", "69"},
    {"116365.27", "18", "7298.08", "19"},
    {"318928.47", "10", "7169.06", "55"},
    {"220987.27", "18", "7700.22", "37"},
    {"442493.07", "4", "9295.83", "51"},
    {"64760.5", "18", "9173.08", "7"},
    {"154748.64", "9", "7118.18", "23"},
    {"344297.73", "3", "5030.66", "75"},
    {"421094.14", "19", "8769.85", "91"},
    {"249353.43", "5", "9358.97", "29"},
    {"430708.79", "12", "8873.3", "67"},
    {"170643.16", "1", "5871.71", "29"},
    {"61524.96", "12", "5633.93", "11"},
    {"113624.87", "1", "8406.85", "13"},
    {"5967.12", "6", "2213.41", "3"},
    {"20849.85", "15", "4236.92", "5"},
    {"388400.51", "1", "8562.77", "47"},
    {"431595.38", "6", "6147.4", "87"},
    {"80682.32", "2", "6390.31", "13"},
    {"363596.35", "4", "6901.2", "59"},
    {"414382.59", "1", "6881.4", "61"},
    {"439333.75", "9", "9996.33", "53"},
    {"196037.34", "1", "4615.5", "43"},
    {"463306.34", "4", "9789.63", "51"},
    {"220207.83", "3", "7092.69", "33"},
    {"152830.62", "1", "3755.56", "41"},
    {"189746.42", "16", "6404.45", "37"},
    {"426403.32", "13", "4778.25", "317"},
    {"9861.75", "11", "1626.75", "7"},
    {"9984.95", "19", "3774.24", "3"},
    {"334354.21", "19", "6324.82", "115"},
    {"197335.49", "5", "3030.43", "77"},
    {"85882.79", "17", "6490.66", "15"},
    {"177323.46", "2", "8332.36", "21"},
    {"7000.37", "4", "5717.24", "1"},
    {"355107.55", "12", "4971.64", "125"},
    {"232670.52", "18", "7548.33", "41"},
    {"427159.84", "14", "9233.23", "67"},
    {"80414.33", "12", "1359.38", "89"},
    {"10000", "0", "1000", "10"},
    {"0", "12", "1000", "0"},
    {"100000", "12", "0", "0"}
  ]

  describe "number_of_payments_left/1" do
    test "should calculate number of payments left" do
      for {principal, interest_rate, monthly_payment, expected} <- @examples do
        assert %{
                 principal: Decimal.new(principal),
                 interest_rate: Decimal.new(interest_rate),
                 monthly_payment: Decimal.new(monthly_payment)
               }
               |> Utils.number_of_payments_left()
               |> Decimal.eq?(expected)
      end
    end
  end

  describe "describe_period" do
    test "should return nil when months is zero" do
      assert nil == Utils.describe_period(0)
    end

    test "should return 1 Month when months is 1" do
      assert "1 Month" == Utils.describe_period(1)
    end

    test "should return the number of months when it less than 12 and greater than 1" do
      for i <- 2..11 do
        assert "#{i} Months" == Utils.describe_period(i)
      end
    end

    test "should return the year when number of months is 12" do
      assert "1 Year" == Utils.describe_period(12)
    end

    test "should return the year number when number of rem of months over 12 is zero" do
      for i <- 2..5 do
        assert "#{i} Years" == Utils.describe_period(12 * i)
      end
    end

    test "should show month number after the year" do
      for i <- 2..5, x <- 2..11 do
        assert "#{i} Years and #{x} Months" == Utils.describe_period(12 * i + x)
      end
    end
  end

  describe "sort_debts/2" do
    test "should sort debts by principal asc when type is snowball" do
      assert {[
                %Debt{
                  principal: 50,
                  payoff_order: 0,
                  id: "957ed733-df59-4905-a6c2-edcb1171e433"
                },
                %Debt{
                  principal: 100,
                  payoff_order: 2,
                  id: "546dfb73-81e8-4225-a425-af4049b891bf"
                },
                %Debt{
                  principal: 150,
                  payoff_order: 4,
                  id: "ac3076d4-f6b4-4fa6-9e10-9573c9f2a6ae"
                },
                %Debt{
                  principal: 200,
                  payoff_order: 5,
                  id: "4a1cef6f-5a72-4a1b-befa-323a48bdfe65"
                }
              ],
              [
                %Mortgage{
                  principal: 90,
                  payoff_order: 1,
                  id: "0cd5db0e-3a5c-4caa-99d4-e58ff3526649"
                },
                %Mortgage{
                  principal: 120,
                  payoff_order: 3,
                  id: "c056e83c-2965-4795-8834-b05e79e521c1"
                },
                %Mortgage{
                  principal: 7000,
                  payoff_order: 6,
                  id: "d0d26da6-40c5-4d1c-b001-0f0b05feb370"
                },
                %Mortgage{
                  principal: 30_000,
                  payoff_order: 7,
                  id: "1cd75885-7135-4cae-b3a2-fe2f3a0432a4"
                }
              ]} =
               Scenarios.sort_debts(
                 [
                   %Debt{principal: 200, id: "4a1cef6f-5a72-4a1b-befa-323a48bdfe65"},
                   %Debt{principal: 100, id: "546dfb73-81e8-4225-a425-af4049b891bf"},
                   %Debt{principal: 150, id: "ac3076d4-f6b4-4fa6-9e10-9573c9f2a6ae"},
                   %Debt{principal: 50, id: "957ed733-df59-4905-a6c2-edcb1171e433"}
                 ],
                 [
                   %Mortgage{principal: 7000, id: "d0d26da6-40c5-4d1c-b001-0f0b05feb370"},
                   %Mortgage{principal: 90, id: "0cd5db0e-3a5c-4caa-99d4-e58ff3526649"},
                   %Mortgage{principal: 120, id: "c056e83c-2965-4795-8834-b05e79e521c1"},
                   %Mortgage{principal: 30_000, id: "1cd75885-7135-4cae-b3a2-fe2f3a0432a4"}
                 ],
                 :debt_snowball
               )
    end

    test "can sort by id if the target value is same" do
      assert {[
                %Debt{
                  principal: 50,
                  payoff_order: 2,
                  id: "4a1cef6f-5a72-4a1b-befa-323a48bdfe65"
                },
                %Debt{
                  principal: 50,
                  payoff_order: 3,
                  id: "546dfb73-81e8-4225-a425-af4049b891bf"
                },
                %Debt{
                  principal: 50,
                  payoff_order: 4,
                  id: "957ed733-df59-4905-a6c2-edcb1171e433"
                },
                %Debt{
                  principal: 50,
                  payoff_order: 5,
                  id: "ac3076d4-f6b4-4fa6-9e10-9573c9f2a6ae"
                }
              ],
              [
                %Mortgage{
                  principal: 50,
                  payoff_order: 0,
                  id: "0cd5db0e-3a5c-4caa-99d4-e58ff3526649"
                },
                %Mortgage{
                  principal: 50,
                  payoff_order: 1,
                  id: "1cd75885-7135-4cae-b3a2-fe2f3a0432a4"
                },
                %Mortgage{
                  principal: 50,
                  payoff_order: 6,
                  id: "c056e83c-2965-4795-8834-b05e79e521c1"
                },
                %Mortgage{
                  principal: 50,
                  payoff_order: 7,
                  id: "d0d26da6-40c5-4d1c-b001-0f0b05feb370"
                }
              ]} =
               Scenarios.sort_debts(
                 [
                   %Debt{principal: 50, id: "4a1cef6f-5a72-4a1b-befa-323a48bdfe65"},
                   %Debt{principal: 50, id: "546dfb73-81e8-4225-a425-af4049b891bf"},
                   %Debt{principal: 50, id: "ac3076d4-f6b4-4fa6-9e10-9573c9f2a6ae"},
                   %Debt{principal: 50, id: "957ed733-df59-4905-a6c2-edcb1171e433"}
                 ],
                 [
                   %Mortgage{principal: 50, id: "d0d26da6-40c5-4d1c-b001-0f0b05feb370"},
                   %Mortgage{principal: 50, id: "0cd5db0e-3a5c-4caa-99d4-e58ff3526649"},
                   %Mortgage{principal: 50, id: "c056e83c-2965-4795-8834-b05e79e521c1"},
                   %Mortgage{principal: 50, id: "1cd75885-7135-4cae-b3a2-fe2f3a0432a4"}
                 ],
                 :debt_snowball
               )
    end

    test "can work with maps with string keys" do
      assert {[
                %{
                  "id" => "4a1cef6f-5a72-4a1b-befa-323a48bdfe65",
                  "payoff_order" => 0,
                  "principal" => 10
                },
                %{
                  "id" => "ac3076d4-f6b4-4fa6-9e10-9573c9f2a6ae",
                  "payoff_order" => 2,
                  "principal" => 20
                },
                %{"payoff_order" => 4, "principal" => 40},
                %{
                  "id" => "546dfb73-81e8-4225-a425-af4049b891bf",
                  "payoff_order" => 6,
                  "principal" => 50
                }
              ],
              [
                %{
                  "id" => "d0d26da6-40c5-4d1c-b001-0f0b05feb370",
                  "payoff_order" => 1,
                  "principal" => 15
                },
                %{
                  "id" => "c056e83c-2965-4795-8834-b05e79e521c1",
                  "payoff_order" => 3,
                  "principal" => 25
                },
                %{"payoff_order" => 5, "principal" => 45},
                %{
                  "id" => "0cd5db0e-3a5c-4caa-99d4-e58ff3526649",
                  "payoff_order" => 7,
                  "principal" => 55
                }
              ]} =
               Scenarios.sort_debts(
                 [
                   %{"principal" => 10, "id" => "4a1cef6f-5a72-4a1b-befa-323a48bdfe65"},
                   %{"principal" => 50, "id" => "546dfb73-81e8-4225-a425-af4049b891bf"},
                   %{"principal" => 20, "id" => "ac3076d4-f6b4-4fa6-9e10-9573c9f2a6ae"},
                   %{"principal" => 40}
                 ],
                 [
                   %{"principal" => 15, "id" => "d0d26da6-40c5-4d1c-b001-0f0b05feb370"},
                   %{"principal" => 55, "id" => "0cd5db0e-3a5c-4caa-99d4-e58ff3526649"},
                   %{"principal" => 25, "id" => "c056e83c-2965-4795-8834-b05e79e521c1"},
                   %{"principal" => 45}
                 ],
                 :debt_snowball
               )
    end

    test "should sort debts by interest rate desc when type is avalanche" do
      assert {[
                %Debt{
                  interest_rate: 22,
                  payoff_order: 0,
                  id: "ac3076d4-f6b4-4fa6-9e10-9573c9f2a6ae"
                },
                %Debt{
                  interest_rate: 18,
                  payoff_order: 1,
                  id: "546dfb73-81e8-4225-a425-af4049b891bf"
                },
                %Debt{
                  interest_rate: 16,
                  payoff_order: 2,
                  id: "957ed733-df59-4905-a6c2-edcb1171e433"
                },
                %Debt{
                  interest_rate: 12,
                  payoff_order: 3,
                  id: "4a1cef6f-5a72-4a1b-befa-323a48bdfe65"
                }
              ],
              [
                %Mortgage{
                  interest_rate: 8,
                  payoff_order: 4,
                  id: "0cd5db0e-3a5c-4caa-99d4-e58ff3526649"
                },
                %Mortgage{
                  interest_rate: 6,
                  payoff_order: 5,
                  id: "1cd75885-7135-4cae-b3a2-fe2f3a0432a4"
                },
                %Mortgage{
                  interest_rate: 2,
                  payoff_order: 6,
                  id: "d0d26da6-40c5-4d1c-b001-0f0b05feb370"
                },
                %Mortgage{
                  interest_rate: 2,
                  payoff_order: 7,
                  id: "c056e83c-2965-4795-8834-b05e79e521c1"
                }
              ]} =
               Scenarios.sort_debts(
                 [
                   %Debt{interest_rate: 12, id: "4a1cef6f-5a72-4a1b-befa-323a48bdfe65"},
                   %Debt{interest_rate: 18, id: "546dfb73-81e8-4225-a425-af4049b891bf"},
                   %Debt{interest_rate: 22, id: "ac3076d4-f6b4-4fa6-9e10-9573c9f2a6ae"},
                   %Debt{interest_rate: 16, id: "957ed733-df59-4905-a6c2-edcb1171e433"}
                 ],
                 [
                   %Mortgage{interest_rate: 2, id: "d0d26da6-40c5-4d1c-b001-0f0b05feb370"},
                   %Mortgage{interest_rate: 8, id: "0cd5db0e-3a5c-4caa-99d4-e58ff3526649"},
                   %Mortgage{interest_rate: 2, id: "c056e83c-2965-4795-8834-b05e79e521c1"},
                   %Mortgage{interest_rate: 6, id: "1cd75885-7135-4cae-b3a2-fe2f3a0432a4"}
                 ],
                 :debt_avalanche
               )
    end
  end

  describe "actual_payoff_order/1" do
    test "should return debt_snowball when the debts sorted by principal asc" do
      assert :debt_snowball =
               Scenarios.actual_payoff_order(
                 [
                   %Debt{
                     principal: 50,
                     payoff_order: 0,
                     id: "957ed733-df59-4905-a6c2-edcb1171e433"
                   },
                   %Debt{
                     principal: 100,
                     payoff_order: 1,
                     id: "546dfb73-81e8-4225-a425-af4049b891bf"
                   },
                   %Debt{
                     principal: 150,
                     payoff_order: 2,
                     id: "ac3076d4-f6b4-4fa6-9e10-9573c9f2a6ae"
                   },
                   %Debt{
                     principal: 200,
                     payoff_order: 3,
                     id: "4a1cef6f-5a72-4a1b-befa-323a48bdfe65"
                   }
                 ],
                 [
                   %Mortgage{
                     principal: 2000,
                     payoff_order: 4,
                     id: "d0d26da6-40c5-4d1c-b001-0f0b05feb370"
                   },
                   %Mortgage{
                     principal: 8000,
                     payoff_order: 5,
                     id: "0cd5db0e-3a5c-4caa-99d4-e58ff3526649"
                   },
                   %Mortgage{
                     principal: 10_000,
                     payoff_order: 6,
                     id: "c056e83c-2965-4795-8834-b05e79e521c1"
                   },
                   %Mortgage{
                     principal: 12_000,
                     payoff_order: 7,
                     id: "1cd75885-7135-4cae-b3a2-fe2f3a0432a4"
                   }
                 ]
               )
    end

    test "should return debt_avalanche when the debts sorted by interest rate desc" do
      assert :debt_avalanche =
               Scenarios.actual_payoff_order(
                 [
                   %Debt{
                     interest_rate: 22,
                     payoff_order: 0,
                     id: "ac3076d4-f6b4-4fa6-9e10-9573c9f2a6ae"
                   },
                   %Debt{
                     interest_rate: 18,
                     payoff_order: 1,
                     id: "546dfb73-81e8-4225-a425-af4049b891bf"
                   },
                   %Debt{
                     interest_rate: 16,
                     payoff_order: 2,
                     id: "957ed733-df59-4905-a6c2-edcb1171e433"
                   },
                   %Debt{
                     interest_rate: 12,
                     payoff_order: 3,
                     id: "4a1cef6f-5a72-4a1b-befa-323a48bdfe65"
                   }
                 ],
                 [
                   %Mortgage{
                     interest_rate: 8,
                     payoff_order: 4,
                     id: "0cd5db0e-3a5c-4caa-99d4-e58ff3526649"
                   },
                   %Mortgage{
                     interest_rate: 6,
                     payoff_order: 5,
                     id: "1cd75885-7135-4cae-b3a2-fe2f3a0432a4"
                   },
                   %Mortgage{
                     interest_rate: 2,
                     payoff_order: 6,
                     id: "c056e83c-2965-4795-8834-b05e79e521c1"
                   },
                   %Mortgage{
                     interest_rate: 1,
                     payoff_order: 7,
                     id: "d0d26da6-40c5-4d1c-b001-0f0b05feb370"
                   }
                 ]
               )
    end

    test "should return custom when the debts randomly sorted" do
      assert :custom =
               Scenarios.actual_payoff_order(
                 [
                   %Debt{interest_rate: 18, id: "546dfb73-81e8-4225-a425-af4049b891bf"},
                   %Debt{interest_rate: 16, id: "957ed733-df59-4905-a6c2-edcb1171e433"},
                   %Debt{interest_rate: 22, id: "ac3076d4-f6b4-4fa6-9e10-9573c9f2a6ae"},
                   %Debt{interest_rate: 12, id: "4a1cef6f-5a72-4a1b-befa-323a48bdfe65"}
                 ],
                 [
                   %Mortgage{interest_rate: 2, id: "d0d26da6-40c5-4d1c-b001-0f0b05feb370"},
                   %Mortgage{interest_rate: 8, id: "0cd5db0e-3a5c-4caa-99d4-e58ff3526649"},
                   %Mortgage{interest_rate: 2, id: "c056e83c-2965-4795-8834-b05e79e521c1"},
                   %Mortgage{interest_rate: 6, id: "1cd75885-7135-4cae-b3a2-fe2f3a0432a4"}
                 ]
               )
    end
  end
end
